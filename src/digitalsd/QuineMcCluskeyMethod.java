package digitalsd;

/**
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class QuineMcCluskeyMethod {
    
    private final GetInput Quine;
    
    //Integers
    private int oneCounter = 0;
    protected int andGateCount;
    protected int OrGateCount;
    private final int[] [] countColumn;
    private boolean [][] checkDominate;
    private int [] count;
    private final int bitsLength;
    
    // ArrayLists
    private final ArrayList <String> solution;
    private final ArrayList <String> minterms;
    private final ArrayList <Integer> dontCares;
    private ArrayList <String> primeImplicant;
    private final ArrayList <String> binaryMinterms;
    private ArrayList <Integer>[] tempCoveredMinterms;
    private final ArrayList <String> temp;
    private final ArrayList <Integer> solutionsIdx;
   
    // Boolean
    private final boolean [][][] state;    
    
    // String
    private final String [] [] [] columns;
    
    // Map
    private final Map<String, CoveredMinterms> saved;
       
    // Constructor
    public QuineMcCluskeyMethod(ArrayList<String> outputs, int convert){
        this.solutionsIdx = new ArrayList<>();
        this.Quine = GetInput.getInstance();
        this.saved = new HashMap<>();
        this.temp = new ArrayList<>();
        this.solution = new ArrayList<>();
        this.minterms = new ArrayList<>();
        this.dontCares = new ArrayList<>();
        this.binaryMinterms = new ArrayList<>();
        this.columns = new String [convert+1] [convert+1] [1000];
        this.state = new boolean [convert+1] [convert+1] [1000];
        this.countColumn = new int [convert+1] [(int)Math.pow(2, convert)];
        bitsLength = convert;
               
        // Find prime Implicants
        findPrimeImplicants(outputs);
        
        // Determining solution
        do{
            getEssentialPrimeImplicants(minterms, primeImplicant);
        
            rowDominance(minterms, primeImplicant);
            
            columnDominance();
        } while(!minterms.isEmpty() && !primeImplicant.isEmpty());
           
        removeDuplicate((ArrayList)solution);
        
    }// end constructor
    
    // Find prime implicants
    private void findPrimeImplicants(ArrayList<String> outputs){
        
        // get minterms to work with
        getMinterms(outputs);
        
        // convert to binary and format to the appropriate number of bits
        convertToBinary(minterms, dontCares);
        
        // perform columns grouping
        columnGrouping();
        
        // Determine prime Implicants
        primeImplicant = getPrimeImplicant();        
    }// end method findPrimeImplicants
    
    // get Minterms from outputs
    private void getMinterms(ArrayList <String> output){
        
        // Convert arrayList into array
        String [] out = new String [output.size()];
        output.toArray(out);
        
        // Select minterms out of all ouputs
        mintermLoop:
        for (int mintermCount = 0; mintermCount < (int)Math.pow(2, bitsLength); mintermCount++) {
            
            switch (out[mintermCount]) {
                case "1": // if number is 1
                    minterms.add(String.valueOf(mintermCount));
                    break;
                    
                case "X":// the value is X
                    dontCares.add(mintermCount);
                    break;
            }// end switch 
        }// end for mintermLoop
    }// end getMinterms

    // Conversion from minterms include don' cares to their Binary representation
    private void convertToBinary(ArrayList<String> MinTDec, ArrayList<Integer> DontDec){

        // convert to binary
        for (int convertCounter = 0; convertCounter < (int)Math.pow(2, bitsLength); convertCounter++) {
            
            if(MinTDec.contains(String.valueOf(convertCounter))){
                binaryMinterms.add(String.format("%0"+bitsLength+"d", Integer.parseInt(Integer.toString(convertCounter, 2))));
                
            } else if (DontDec.contains(convertCounter)){
                binaryMinterms.add(String.format("%0"+bitsLength+"d", Integer.parseInt(Integer.toString(convertCounter, 2))));
            }// end if
        }// end for
    }// end method convertToBinary

    // Grouping
    private void column0grouping(){
        
        //convert mintems in binary
        outerLoop:
        for (String binaryMinterm : binaryMinterms) {

            // count number on ones in the minterm
            counter(binaryMinterm, "1");
            innerLoop:
            for (int j = 0; j <= bitsLength; j++) {
                if (oneCounter == j) {
                    
                    // Store values of column 0
                    columns[0][oneCounter][countColumn[0][j]] = binaryMinterm;
                    
                    // increment column 0 counter
                    countColumn[0][j]++;
                } // end if
            } // end innerLoop
            
            // Reinitialize the counter
            oneCounter = 0;
        } // end for outerLoop
    }// end method column0grouping
    
    // group the remaining columns
    private void columnGrouping(){
        
        // Group minterms according to the number of one they contain...
        column0grouping();

        // Preparing for grouping of others columns
        
        // bit counter
        bitLoop:
        for (int bitCount = 1; bitCount <= Quine.numberOfBits; bitCount++) {
            
            // group of 1's counter
            groupOfOnesLoop:
            for (int groupOneCounter = 0; groupOneCounter < countColumn[bitCount-1].length; groupOneCounter++) {

                // current value counter of the the group of one's
                currentValueLoop:
                for (int currentValueCounter = 0; currentValueCounter < countColumn[bitCount-1][groupOneCounter]; currentValueCounter++) {
                    
                    // split first minterms to be compared
                    String [] currentMinterms = columns[bitCount-1][groupOneCounter][currentValueCounter].split("");
                    
                    // next value counter of the adjacent group one's
                    nextValueLoop:
                    for (int nextValueCounter = 0; nextValueCounter < countColumn[bitCount-1][groupOneCounter+1]; nextValueCounter++) {
                        
                        // split second minterms to be compared
                        String [] nextadjacentMinterms = columns[bitCount-1][groupOneCounter+1][nextValueCounter].split("");
                        
                        if(comparingAdjacentMinterns(currentMinterms, nextadjacentMinterms) != null){
                            
                            // Store in the next column table
                            columns[bitCount][groupOneCounter][countColumn[bitCount][groupOneCounter]] = comparingAdjacentMinterns(currentMinterms, nextadjacentMinterms);
                            
                            // set State of compared numbers
                            state [bitCount-1][groupOneCounter][currentValueCounter] = true;
                            state [bitCount-1][groupOneCounter+1][nextValueCounter] = true;
                            
                            
                            // Store minterms covered by both minterms
                            coveredMinterms(columns[bitCount][groupOneCounter][countColumn[bitCount][groupOneCounter]], 
                                    columns[bitCount-1][groupOneCounter][currentValueCounter], 
                                    columns[bitCount-1][groupOneCounter+1][nextValueCounter]);
                            
                            // increment counter for the next column
                            countColumn[bitCount][groupOneCounter]++;
                            
                        }// end if
                    }// end for nextValueLoop
                }// end for currentValueLoop
            }// end for groupOfOnesLoop
        }// end for columnLoop
    }// end method columnGrouping
    
    // Comparing adjacents mintermsfor column grouping
    private String comparingAdjacentMinterns(String [] value1, String [] value2){
        String result = "" ;
        int counter = 0;
        
        for (int i = 0; i < value1.length; i++) {
            if(value1[i].equals(value2[i])){
                result += value1[i];
                counter++;
                
            } else{
                result += "-";
            }// end if
        }// end for
        
        if(counter == value1.length - 1){
           return result; 
        }// end for
        
        return null;
    }// end method comparingAdjacentMinterns

    // Get possible solution
    private ArrayList <String> getPrimeImplicant(){
        
        temp.clear();
        
        // determine raw prime implicants for solution to refine later
        for (int i = 0; i < countColumn.length; i++) {
            for (int j = 0; j < countColumn[i].length; j++) {
                for (int k = 0; k < countColumn[i][j]; k++) {
                    
                    // Store prime implicant which are values that are not paired
                    if(columns[i][j][k] != null && state[i][j][k] == false){
                        temp.add(columns[i][j][k]);
                    }// end if
                }// end for
            }// end for
        }// end for  
        
        // Removing duplicate to obtaining Prime Implicants  
        return removeDuplicate((ArrayList)temp);
    }// end method reducePrimeImplicant
   
    // Remove Duplicate in solution
    private ArrayList <Object> removeDuplicate(ArrayList <Object> list){
        
        // Removing duplicate to obtaining Prime Implicants
        Set<Object> hs = new HashSet<>();
        hs.addAll(list);
        list.clear();
        list.addAll(hs);
        
        return list;
    }// end method removeDuplicate
    
    // Get implicant table
    private void implicantTable(ArrayList <String> minT, ArrayList<String> PICovering){
   
        count = new int [minT.size()];
        checkDominate = new boolean [minT.size()][PICovering.size()];
        tempCoveredMinterms = new ArrayList[minT.size()];
        
        // Implicant table
        for (int i = 0; i < minT.size(); i++) {
            for (int j = 0; j < PICovering.size(); j++) {
                for (Map.Entry<String, CoveredMinterms> entrySet : saved.entrySet()) {
                    String currentPI = entrySet.getKey();
                    CoveredMinterms coveringCurrentPI = entrySet.getValue();

                    // count number of one in a row
                    if (PICovering.get(j).equals(currentPI)) {
                        if (coveringCurrentPI.coveringNumbers.contains(Integer.parseInt(minT.get(i)))) {
                            tempCoveredMinterms[i] = coveringCurrentPI.coveringNumbers;
                            checkDominate[i][j] = true;
                            count[i]++;
                        }// end if
                    }// end if
                }// end for
            }// end for
        }// end for
                
    }// end method 
    
    // Determine Essential Prime Implicants
    private void getEssentialPrimeImplicants(ArrayList <String> minT, ArrayList<String> PICovering){
        
        // determine implicant table
        implicantTable(minT, PICovering);
        
        // store essential prime implicants if counter is 1
        for (int i = 0; i < count.length; i++) {
            if(count[i] == 1){
                for (Map.Entry<String, CoveredMinterms> entrySet : saved.entrySet()) {
                    String currentPI = entrySet.getKey();
                    CoveredMinterms coveringCurrentPI = entrySet.getValue();
                    
                    if(tempCoveredMinterms[i] == coveringCurrentPI.coveringNumbers){
                        solution.add(currentPI);
                        solutionsIdx.addAll(coveringCurrentPI.coveringNumbers);
                    }// end if
                }// end for
            }// end if
        }// end for
        
        //reduce Row
        removeRowCoveredByEssentialPIs(minT, PICovering, solution);
        
    }// end method getEssentialPrimeImplicants
    
    private void removeRowCoveredByEssentialPIs(ArrayList <String> minT, ArrayList<String> PICovering, ArrayList<String> essentialPI){
        // Local Variable
        ArrayList<String> toRemove = new ArrayList<>();
        
        // Remove element covered by Essential Prime Implicant
        for (int i = 0; i < minT.size(); i++) {
            for (int j = 0; j < PICovering.size(); j++) {
                for (String essentialPI1 : essentialPI) {
                    if (essentialPI1.equals(PICovering.get(j))) {
                        if(checkDominate[i][j]){
                            toRemove.add(minT.get(i));
                        }// end if
                    }// end if
                }// end for
            }// end for
        }// end for
        
        minT.removeAll(toRemove); // remove minterms
        
        PICovering.removeAll(essentialPI); // remove Prime Implcants
    }// end method removeRowCoveredByEssentialPIs

    // Remove uncessary rows
    private void rowDominance(ArrayList <String> minT, ArrayList<String> PICovering){
        // Local variables
        ArrayList <String> tempMintermsToRemoved = new ArrayList<>();
                
        // determine implicants table
        implicantTable(minT, PICovering);
    
        // Determine which to remove
        for (int i = 0; i < minT.size(); i++) {
            if(count[i] == 0 && !tempMintermsToRemoved.contains(minT.get(i))){
                tempMintermsToRemoved.add(minT.get(i));
                
            } else{
                for (int j = 0; j < PICovering.size(); j++) {
                    for (int k = 0; k < minT.size(); k++) {
                        if(count[k] == 0 && !tempMintermsToRemoved.contains(minT.get(k))){
                            tempMintermsToRemoved.add(minT.get(k));
                            
                        } else{
                            for (int l = 0; l < PICovering.size(); l++) {
                                if(i !=k ){
                                    if(j == l ){
                                        if( count[i] > count[k] && count[i] != minT.size() && count[k] != minT.size()){
                                            if(checkDominate[i][j] && checkDominate[k][l]){
                                                if(!tempMintermsToRemoved.contains(minT.get(i))){
                                                    tempMintermsToRemoved.add(minT.get(i));
                                                    
                                                }// end if
                                            }// end if
                                        }// end if
                                        else if( count[i] == count[k] && j != l){
                                            if(checkDominate[i][j] && checkDominate[k][l]){
                                                if(!tempMintermsToRemoved.contains(minT.get(i))){
                                                    tempMintermsToRemoved.add(minT.get(i));
                                                    
                                                }// end if
                                            }// end if
                                        }// end if
                                    }// end if
                                }// end if
                            }// end for
                        }// end if
                    }// end for
                }// end for
            }// end if
        }// end for
        
        minT.removeAll(tempMintermsToRemoved);
    }// end method rowDominance
    
    private void columnDominance(){
        // Local variables
        ArrayList <String> tempPrimeImpTORemoved = new ArrayList<>();
        ArrayList <String> PrimeImpCompared = new ArrayList<>();
        count = new int [primeImplicant.size()];
        checkDominate = new boolean [primeImplicant.size()][minterms.size()];
        
        // Implicant table
        for (int i = 0; i < primeImplicant.size(); i++) {
            for (int j = 0; j < minterms.size(); j++) {
                for (Map.Entry<String, CoveredMinterms> entrySet : saved.entrySet()) {
                    String currentPI = entrySet.getKey();
                    CoveredMinterms coveringCurrentPI = entrySet.getValue();
                    
                    if(primeImplicant.get(i).equals(currentPI)){
                        if(coveringCurrentPI.coveringNumbers.contains(Integer.parseInt(minterms.get(j)))){
                            checkDominate[i][j] = true;
                            count[i]++;
                        }// end if
                    }// end if
                }// end for
            }// end for
        }// end for
        
        // Eliminate prime Implicants
        for (int i = 0; i < primeImplicant.size(); i++) {
            if(count[i] == 0 && !tempPrimeImpTORemoved.contains(primeImplicant.get(i))){
                tempPrimeImpTORemoved.add(primeImplicant.get(i));

            } else{
                for (int j = 0; j < minterms.size(); j++) {
                    for (int k = 0; k < primeImplicant.size(); k++) {
                        if(count[k] == 0 && !tempPrimeImpTORemoved.contains(primeImplicant.get(k))){
                            tempPrimeImpTORemoved.add(primeImplicant.get(k));
                            
                        } else{
                            for (int l = 0; l < minterms.size(); l++) {
                                if(count[i] == count[k] && i != k && j == l){
                                    if(checkDominate[i][j] && checkDominate[k][l]){
                                        if(!tempPrimeImpTORemoved.contains(primeImplicant.get(k)) && !PrimeImpCompared.contains(primeImplicant.get(i))){
                                            tempPrimeImpTORemoved.add(primeImplicant.get(k));
                                            
                                            // Stored PI compared already
                                            PrimeImpCompared.add(primeImplicant.get(i));
                                            PrimeImpCompared.add(primeImplicant.get(k));
                                        }// end if
                                    }// end if
                                    
                                } else if(count[i] > count[k] && i != k && j == l){
                                    if(checkDominate[i][j] && checkDominate[k][l]){
                                        if(!tempPrimeImpTORemoved.contains(primeImplicant.get(k)) && !PrimeImpCompared.contains(primeImplicant.get(i))){
                                            tempPrimeImpTORemoved.add(primeImplicant.get(k));
                                            
                                            // Stored PI compared already
                                            PrimeImpCompared.add(primeImplicant.get(i));
                                            PrimeImpCompared.add(primeImplicant.get(k));
                                        }// end if
                                    }// end if
                                }// end if
                            }// end for
                        }// end if
                    }// end for
                }// end for
            }// end if
        }// end for
        
        primeImplicant.removeAll(tempPrimeImpTORemoved);
    }// end method columnDominance
    
    /**
     * extractExpression method will Extract solution to proper variable name
     * @return
     */
    protected final String extractExpression(){
        
        // Local Variables
        String [] SplitExpression;
        String logicExpression = "";
        int counter =0;

        // Expression is Vcc
        if(binaryMinterms.size() == (int)Math.pow(2, bitsLength)){
            logicExpression = "1";
            solutionsIdx.clear();
            for (int i = 0; i < binaryMinterms.size(); i++) {
                solutionsIdx.add(i);
            }// end for
        } else if(solution.isEmpty()){
            logicExpression = "0";
            
        } else {
            solutionLoop:
            for (int solutionCounter = 0; solutionCounter < solution.size(); solutionCounter++) {
                
                // split each solution elements
                SplitExpression = solution.get(solutionCounter).split("(?!^)");
                
                for (int variableNameCounter = 0; variableNameCounter < SplitExpression.length; variableNameCounter++) {
                    switch (SplitExpression[variableNameCounter]) {
                        case "0":
                            if(variableNameCounter == SplitExpression.length - 1 && Quine.numberOfBits == 3 && Quine.cycle.equals("Up/Down Counter")){
                                logicExpression = logicExpression.concat(Quine.subHeaderInputsNames()[3]+"'");
                                
                            } else{
                                logicExpression = logicExpression.concat(Quine.subHeaderInputsNames()[variableNameCounter]+"'");
                            }// end if
                            
                            counter++;
                            break;

                        case "1":
                            if(variableNameCounter == SplitExpression.length - 1 && Quine.numberOfBits == 3 && Quine.cycle.equals("Up/Down Counter")){ 
                                logicExpression = logicExpression.concat(Quine.subHeaderInputsNames()[3]);
                                
                            } else{
                                logicExpression = logicExpression.concat(Quine.subHeaderInputsNames()[variableNameCounter]);
                            }// end if
                            
                            counter++;
                            break;
                    }// end switch
                }// end for
                
                // counting number of And gate required
                if(counter > 1){
                    andGateCount++;
                }// end if
                
                // add sign plus while last elements of solution is not reached
                if(solutionCounter != solution.size() - 1){
                    logicExpression = logicExpression.concat(" + ");
                    OrGateCount++;
                }// end if
                counter = 0;
                
            }// end for solutionLoop
            removeDuplicate((ArrayList)solutionsIdx);
        }// end if
        
        return logicExpression;
    }// end method extractExpression
    
    // Method to counter one of minterms
    private void counter( String elemenentToCount, String elementCounted){
        
        String [] toSplit = elemenentToCount.split("(?!^)");
        
        for (int count = 0; count < toSplit.length; count++) {
            if(toSplit[count].equals(elementCounted)){
                oneCounter++;
                if(elementCounted.equals("-")){
                    temp.add(String.valueOf(count));
                }// end if
            }// end if
        }// end for
    }// end method counter
    
    // Recover covered minterms
    private void coveredMinterms(String key, String mint1, String mint2){

        saved.put(key, new CoveredMinterms(binaryToDecimal(mint1), binaryToDecimal(mint2)));
        
    }// end method coveredMinterms
    
    // Convert binary or minterms to correspondant decimal value
    private ArrayList<Integer> binaryToDecimal(String t){
        // Local variable
        ArrayList<Integer> binToDec = new ArrayList<>();
        
        counter(t, "-");
        String res ;
        if(oneCounter == 1){
            
            // get minterms that paired each other to give this number and convert them to decimal value.
           binToDec.add(Integer.parseInt(t.replace("-", "1"), 2));
           binToDec.add(Integer.parseInt(t.replace("-", "0"), 2));
           
        } else if(oneCounter == 2){
            res = "";
            String [] spliter = t.split("(?!^)");
            
            for (int i = 0; i < spliter.length; i++) {
                if(temp.get(0).equals(String.valueOf(i))){
                    res += "0";
                } else if(temp.get(1).equals(String.valueOf(i))){
                    res += "0";
                } else{
                    res += spliter[i];
                }// end if
            }// end for
            binToDec.add(Integer.parseInt(res, 2));
            
            res ="";
            for (int i = 0; i < spliter.length; i++) {
                
                if(temp.get(0).equals(String.valueOf(i))){
                    res += "0";
                } else if(temp.get(1).equals(String.valueOf(i))){
                    res += "1";
                } else{
                    res += spliter[i];
                }// end if
            }// end for
            binToDec.add(Integer.parseInt(res, 2));
            
            res ="";
            for (int i = 0; i < spliter.length; i++) {
                
                if(temp.get(0).equals(String.valueOf(i))){
                    res += "1";
                } else if(temp.get(1).equals(String.valueOf(i))){
                    res += "0";
                } else{
                    res += spliter[i];
                }// end if
            }// end for
            binToDec.add(Integer.parseInt(res, 2));
            
            res ="";
            for (int i = 0; i < spliter.length; i++) {
                
                if(temp.get(0).equals(String.valueOf(i))){
                    res += "1";
                } else if(temp.get(1).equals(String.valueOf(i))){
                    res += "1";
                } else{
                    res += spliter[i];
                }// end if
            }// end for
            binToDec.add(Integer.parseInt(res, 2));
            
        } else if(oneCounter == 3){
            res = "";
            String [] spliter = t.split("(?!^)");
            
            for (int i = 0; i < spliter.length; i++) {
                
                if(temp.get(0).equals(String.valueOf(i))){
                    res += "0";
                } else if(temp.get(1).equals(String.valueOf(i))){
                    res += "0";
                } else if(temp.get(2).equals(String.valueOf(i))){
                    res += "0";
                } else{
                    res += spliter[i];
                }// end if
            }// end for
            binToDec.add(Integer.parseInt(res, 2));
            
            res = "";            
            for (int i = 0; i < spliter.length; i++) {
                
                if(temp.get(0).equals(String.valueOf(i))){
                    res += "0";
                } else if(temp.get(1).equals(String.valueOf(i))){
                    res += "0";
                } else if(temp.get(2).equals(String.valueOf(i))){
                    res += "1";
                } else{
                    res += spliter[i];
                }// end if
            }// end for
            binToDec.add(Integer.parseInt(res, 2));
            // ------------------------------           
            res ="";
            for (int i = 0; i < spliter.length; i++) {
                
                if(temp.get(0).equals(String.valueOf(i))){
                    res += "0";
                } else if(temp.get(1).equals(String.valueOf(i))){
                    res += "1";
                } else if(temp.get(2).equals(String.valueOf(i))){
                    res += "0";
                } else{
                    res += spliter[i];
                }// end if
            }// end for
            binToDec.add(Integer.parseInt(res, 2));
            
            res ="";
            for (int i = 0; i < spliter.length; i++) {
                
                if(temp.get(0).equals(String.valueOf(i))){
                    res += "0";
                } else if(temp.get(1).equals(String.valueOf(i))){
                    res += "1";
                } else if(temp.get(2).equals(String.valueOf(i))){
                    res += "1";
                } else{
                    res += spliter[i];
                }// end if
            }// end for
            binToDec.add(Integer.parseInt(res, 2));
            // ----------------------------------------------------
            res ="";
            for (int i = 0; i < spliter.length; i++) {
                
                if(temp.get(0).equals(String.valueOf(i))){
                    res += "1";
                } else if(temp.get(1).equals(String.valueOf(i))){
                    res += "0";
                } else if(temp.get(2).equals(String.valueOf(i))){
                    res += "0";
                } else{
                    res += spliter[i];
                }// end if
            }// end for
            binToDec.add(Integer.parseInt(res, 2));
            
            res ="";
            for (int i = 0; i < spliter.length; i++) {
                
                if(temp.get(0).equals(String.valueOf(i))){
                    res += "1";
                } else if(temp.get(1).equals(String.valueOf(i))){
                    res += "0";
                } else if(temp.get(2).equals(String.valueOf(i))){
                    res += "1";
                } else{
                    res += spliter[i];
                }// end if
            }// end for
            binToDec.add(Integer.parseInt(res, 2));
            // ----------------------------------------
            
            res ="";
            for (int i = 0; i < spliter.length; i++) {
                
                if(temp.get(0).equals(String.valueOf(i))){
                    res += "1";
                } else if(temp.get(1).equals(String.valueOf(i))){
                    res += "1";
                } else if(temp.get(2).equals(String.valueOf(i))){
                    res += "0";
                } else{
                    res += spliter[i];
                }// end if
            }// end for
            binToDec.add(Integer.parseInt(res, 2));
            
            res ="";
            for (int i = 0; i < spliter.length; i++) {
                
                if(temp.get(0).equals(String.valueOf(i))){
                    res += "1";
                } else if(temp.get(1).equals(String.valueOf(i))){
                    res += "1";
                } else if(temp.get(2).equals(String.valueOf(i))){
                    res += "1";
                } else{
                    res += spliter[i];
                }// end if
            }// end for
            binToDec.add(Integer.parseInt(res, 2));
 
        } else{
            
            binToDec.add(Integer.parseInt(t,2)) ; 
        }// end if
        
        oneCounter = 0;
        temp.clear();
        return binToDec;
    } // end method binaryToDecimal
    
    protected ArrayList <Integer> getSolutionidx(){
        return solutionsIdx;
    }
}// end class QuineMcCluskeyMethod
