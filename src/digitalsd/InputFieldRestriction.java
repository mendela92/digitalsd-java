
package digitalsd;

/**
 * Piece of Code taken from java2s.com with unknown author
 * Link: http:// www.java2s.com/Tutorial/java/0240_Swing/LimitJTextFieldinputtoamaximum.htm
 * 
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */

import java.awt.Toolkit;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/*
*   This class is destine to limit the inputs digits in a textfield.
*/
class InputFieldRestriction extends PlainDocument {
    
    // Instance variable
    private final int limit;

    // Constructor
    public InputFieldRestriction(int limit) {
      super();
      this.limit = limit;
    }// end constructor

    @Override
    public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
      if (str == null)
        return;

      // allowing input value to the specific limit
      if ((getLength() + str.length()) <= limit) {
        super.insertString(offset, str, attr);

      } else { // if limit exceeded notify user
          Toolkit.getDefaultToolkit().beep();

      }// end if
    }// end method insertString
}//end class 

