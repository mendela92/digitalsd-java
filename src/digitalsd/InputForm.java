package digitalsd;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * DigitalSD
 *
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */
public class InputForm extends javax.swing.JFrame {

    // Instance varaibles
    private final GetInput storeInput;

    private JTextField[] sequenceFields;
    private JTextField optionFieldInput;

    private final ArrayList<Integer> error;

    private String errLocation = "";

    private int currentField;
    private int maxValue = 0;

    /**
     * Constructor
     */
    public InputForm() {

        this.error = new ArrayList<>();
        this.storeInput = GetInput.getInstance();
        this.optionFieldInput = new JTextField();

        // set Icon to the App
        setIcon();

        // Generated components
        initComponents();
    }// end constructor

    //Set sequence Field to the chosen number
    private void FieldInput(final int n) {

        // Initialize variable
        sequenceFields = new JTextField[n];

        Thread generateField = new Thread() {

            GridBagConstraints gridBagConstraints;

            @Override
            public void run() {

                // Maje sure panel is clear before adding components
                sequenceInputPanel.removeAll();

                // Dynamically create inputs fileds to enter sequence number
                inputFieldLoop:
                for (int fieldCount = 0; fieldCount < n; fieldCount++) {

                    // Field input initialize
                    sequenceFields[fieldCount] = new JTextField();
                    sequenceFields[fieldCount].setPreferredSize(new Dimension(35, 25));             // limit size of fields
                    sequenceFields[fieldCount].setBackground(new Color(255, 51, 51));               // set background color of fields
                    sequenceFields[fieldCount].setForeground(new Color(0, 0, 0));                   // set Foregound color of the fields
                    sequenceFields[fieldCount].setFont(new Font("Cambria", Font.BOLD, 16));  // set Font of fields

                    // Set restriction of input to a single char of only
                    if (numberOfBitsComboBox.getSelectedIndex() == 1) {
                        sequenceFields[fieldCount].setDocument(new InputFieldRestriction(1));   // Limit field to allow maximum of one input
                    } else {
                        sequenceFields[fieldCount].setDocument(new InputFieldRestriction(2));   // Limit field to allow maximum of two inputs
                    }// end if

                    sequenceFields[fieldCount].setHorizontalAlignment(javax.swing.JTextField.CENTER);// set align to center in fields

                    //Position of Fields
                    gridBagConstraints = new GridBagConstraints();
                    gridBagConstraints.gridx = fieldCount;
                    gridBagConstraints.gridy = 0;
                    gridBagConstraints.ipadx = 15;
                    gridBagConstraints.anchor = GridBagConstraints.WEST;
                    gridBagConstraints.insets = new Insets(0, 1, 5, 0);

                    // add keyListener to sequence field inputs
                    sequenceFields[fieldCount].addKeyListener(new KeyAdapter() {

                        @Override
                        public void keyReleased(KeyEvent e) {
                            sequenceFieldsKeyPressed(e);
                        }// end method keyReleased
                    });

                    // add mouseListener to sequence field inputs
                    sequenceFields[fieldCount].addMouseListener(new MouseAdapter() {

                        @Override
                        public void mouseReleased(MouseEvent e) {
                            sequenceFieldsMouseReleased(e);
                        }// end method mouseReleased
                    });

                    // add field to panel
                    sequenceInputPanel.add(sequenceFields[fieldCount], gridBagConstraints);

                }// end for inputFieldLoop

                // valide new components
                sequenceInputPanel.validate();

                // set cursor to the first field generated
                sequenceFields[0].requestFocus();
                
                if (numberOfBitsComboBox.getSelectedIndex() == 1) {// 3-Bits selection (0-7)
                    maxValue = 7;
                    
                } else if (numberOfBitsComboBox.getSelectedIndex() == 2) { // 4-Bits Selection (0-15)
                    maxValue = 15;
                    
                }// end if

            }// end inner method run
        };
        generateField.start();

    }// end method FieldInput

    // Validate all input enter by the users
    private void captureInputs() {

        if (isNumberOfBitsSelected()) { // check if number of bits is selected

            if (inputComboBox.getSelectedIndex() != 0) {// index of comboBox different from 0 go to the next check 

                if (!isFieldsEmpty()) { // check if any field is empty

                    if (isValueInteger()) { // Check if value entered is an integer

                        if (!isValueOutOfRange()) { // check if the value(s) are out of range

                            if (!isSequenceDuplicated()) { // Check numbers equence

                                if (isCounterSelected()) { // check if a type of counter has been been selected

                                    if (optionComboBox.getSelectedIndex() == 0) { // SequenceOnly option selected
                                        storeInputs(); //Store innputs from users
                                        outputGUI(); // close Input GUI and open outputs GUI

                                    } else if (optionComboBox.getSelectedIndex() == 1) { // Starter option selected

                                        if (isStarter()) { // Check if corrected starter number

                                            storeInputs(); //Store innputs from users
                                            outputGUI(); // close Input GUI and open outputs GUI
                                        }// end if check starter

                                    } else if (optionComboBox.getSelectedIndex() == 2) { // illegal State selected

                                        if (isIllegalState()) { // Check if correct illegal state number enetered

                                            storeInputs(); //Store innputs from users
                                            outputGUI(); // close Input GUI and open outputs GUI
                                        }// end if
                                    }//end if

                                } else {
                                    cycleComboBox.requestFocus();
                                    Toolkit.getDefaultToolkit().beep();
                                    storeInput.displayMessage("Please Select a type of counter", "TYPE OF COUNTER ERROR!!!");
                                }// end id isCounterSelected

                            } else {
                                if (error.size() == 2) { // display error for the first two Same fields

                                    // display error message 
                                    Toolkit.getDefaultToolkit().beep();
                                    storeInput.displayMessage("Sequence Fields " + error.get(0) + " and " + error.get(1)
                                            + " are equals, Can not have two same numbers in a sequence", "DUPLICATE ERROR!!!");

                                    // Set Cursor to field error
                                    sequenceFields[error.get(0)-1].setText("");
                                    sequenceFields[error.get(1)-1].setText("");
                                    sequenceFields[error.get(0)-1].requestFocus();

                                }//end if 
                            }// end if isSequenceDuplicated
                        }// end if isValueOutOfRange
                        else {
                            if (error.size() == 1) { // if there is only one error

                                // display error message 
                                Toolkit.getDefaultToolkit().beep();
                                storeInput.displayMessage("Please Check your Sequence, Value in Sequence Field " + error.get(0)
                                        + " is out of range, choose Values between (0 - " + maxValue + ") , ", "OUT OF RANGE ERROR!!!");

                                sequenceFields[error.get(0) - 1].requestFocus();    // Set Cursor to field error
                                sequenceFields[error.get(0) - 1].setText("");       // clean fields with error
                            }// end if
                            else if (error.size() > 1) { // if there is more than one field empty

                                //Prepare locations of field errors
                                for (Integer error1 : error) {
                                    errLocation += error1 + ", ";
                                } // end for

                                // display error message 
                                Toolkit.getDefaultToolkit().beep();
                                storeInput.displayMessage("Please Check your Sequence, Value in Sequence Field "
                                        + errLocation.substring(0, errLocation.length() - 2) + " are out of range, please choose between (0 - " + maxValue + ") , ", "OUT OF RANGE ERROR!!!");

                                // clean fields with error
                                for (Integer error1 : error) {
                                    sequenceFields[error1 - 1].setText("");
                                }// end for

                                // Set Cursor to first field error
                                sequenceFields[error.get(0) - 1].requestFocus();

                            }// end if
                        }// end if 

                    } else {
                        if (error.size() == 1) { // if there is only one error

                            // display error message 
                            Toolkit.getDefaultToolkit().beep();
                            storeInput.displayMessage("Did not enter correct input by Sequence Field " + error.get(0)
                                    + ", Please Check your Sequence", "NOT AN INTEGER ERROR!!!");

                            sequenceFields[error.get(0) - 1].setText("");    // clean field with error
                            sequenceFields[error.get(0) - 1].requestFocus();    // Set Cursor to field error
                        }// end if 
                        else if (error.size() > 1) { // if there is more than one field empty

                            //Prepare locations of field errors
                            for (Integer error1 : error) {
                                errLocation += error1 + ", ";
                            }// enf for

                            // display error message 
                            Toolkit.getDefaultToolkit().beep();
                            storeInput.displayMessage("Did not enter correct input by Sequence Fields " + errLocation.substring(0, errLocation.length() - 2)
                                    + ", Please Check your Sequence", "NOT AN INTEGER ERROR!!!");

                            // clean fields with error
                            error.forEach((error1) ->
                            {
                                sequenceFields[error1 - 1].setText("");
                            }); // end for

                            // Set Cursor to first field error
                            sequenceFields[error.get(0) - 1].requestFocus();

                        }// end if
                    }// end if isFieldContainingInteger

                } else {

                    if (error.size() == 1) { // if there is only one error

                        // display error message 
                        Toolkit.getDefaultToolkit().beep();
                        storeInput.displayMessage("Sequence Input Field " + error.get(0) + " is Empty, Please enter the value",
                                "EMPTY FIELD ERROR!!!");

                        // Set Cursor to field error
                        sequenceFields[error.get(0) - 1].requestFocus();

                    }// end if
                    else if (error.size() > 1) { // if there is more than one field empty

                        //Prepare locations of field errors
                        for (Integer error1 : error) {
                            errLocation += error1 + ", ";
                        } // end for

                        // display error message
                        Toolkit.getDefaultToolkit().beep();
                        storeInput.displayMessage("Sequence Input Field " + errLocation.substring(0, errLocation.length() - 2)
                                + " are Empty, Please enter values",
                                "EMPTY FIELD ERROR!!!");

                        // Set Cursor to first field error
                        sequenceFields[error.get(0) - 1].requestFocus();

                    }// end if

                }// end if isFieldEmpty
            } else {

                // display error message
                Toolkit.getDefaultToolkit().beep();
                storeInput.displayMessage("No Input has made into the app, Please Start inputting by Choosing a Number of inputs", "NO INPUT ERROR!!!");

                inputComboBox.requestFocus();
            }// end if check of input comboBox
        }// end if
    }// end method captureInputs

    private void storeInputs() {

        storeInput.decimalSequence.clear();
        // Get sequence numbers
        for (int storeSeqCount = 0; storeSeqCount < storeInput.numberOfInputs; storeSeqCount++) {
            storeInput.decimalSequence.add(sequenceFields[storeSeqCount].getText());
        }// end for

        // Store the number of bits of the sequence
        storeInput.numberOfBits = Integer.parseInt(numberOfBitsComboBox.getSelectedItem().toString().substring(0, 1));

        // get type of cycle
        storeInput.cycle = cycleComboBox.getSelectedItem().toString();

        // get option choosen
        storeInput.optionChoosen = optionComboBox.getSelectedIndex();

        // Storing value of the option selected
        if (optionComboBox.getSelectedIndex() != 0) {
            storeInput.sequenceOptionField = optionFieldInput.getText();
        }// end if
    }// end method storeInputs

    // Calling output gui
    private void outputGUI() {

        dispose();// close open windows
        final OutputForm gui = new OutputForm(); // Instantiation of GUI1

        // Setting to get current Screen size informations 
        GraphicsConfiguration config = gui.getGraphicsConfiguration();
        final int left = Toolkit.getDefaultToolkit().getScreenInsets(config).left;
        final int right = Toolkit.getDefaultToolkit().getScreenInsets(config).right;
        final int top = Toolkit.getDefaultToolkit().getScreenInsets(config).top;
        final int bottom = Toolkit.getDefaultToolkit().getScreenInsets(config).bottom;
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        final int width = screenSize.width - left - right;
        final int height = screenSize.height - top - bottom;

        gui.setSize(width, height); // Size the frame
        gui.setResizable(false); // denied window resizable
        gui.setVisible(true); // set window frame visible
        gui.setLocationRelativeTo(null); // set Window appearance in the middle of the Screen

    }// end method outputGUI

    // set Option of the Sequence counter
    private void counterOption() {

        Thread generateOptionField = new Thread() {

            @Override
            public void run() {

                // if index is 0
                if (optionComboBox.getSelectedIndex() == 0) {

                    //remove option input field if there is one
                    optionFieldPanel.remove(optionFieldInput);

                } else {

                    //remove option input field if there is one
                    optionFieldPanel.remove(optionFieldInput);

                    // add new  option input field, just to make sure that two component are not generated at the same place
                    optionFieldInput = new JTextField();
                    optionFieldInput.setForeground(new Color(0, 0, 0));                     // set background of field
                    optionFieldInput.setBackground(new Color(255, 51, 51));                 // set foreground of field
                    optionFieldInput.setPreferredSize(new Dimension(35, 25));               // Limit size of the field

                    if (numberOfBitsComboBox.getSelectedIndex() == 1) {
                        optionFieldInput.setDocument(new InputFieldRestriction(1));         // Limit input in the field to 1
                    } else {
                        optionFieldInput.setDocument(new InputFieldRestriction(2));         // Limit input in the field to 2
                    }
                    optionFieldInput.setFont(new Font("Cambria", Font.BOLD, 16));           // set font on the field
                    optionFieldInput.setHorizontalAlignment(JTextField.CENTER);             // Center input in the field

                    optionFieldInput.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyPressed(java.awt.event.KeyEvent evt) {
                            optionFieldInputKeyPressed(evt);
                        }// end method keyPressed
                    });

                    // add option input field
                    optionFieldPanel.add(optionFieldInput);
                    optionFieldInput.requestFocus();                                        // set focus to that component

                }// end if

                // repaint component of the Frame
                inputPanel.validate();
            }// end if
        };
        generateOptionField.start();

    }// end method counterOption

    // 
    private void toBeSaved() {

        // Open JFileChooser in the digitalSD directory
        JFileChooser saveFile = new JFileChooser(storeInput.dsdDir);
        FileNameExtensionFilter typeFormat = new FileNameExtensionFilter("DSD File", "dsd"); // dsd format
        saveFile.setFileFilter(typeFormat); // set filter to only allow dsd file
        saveFile.setAcceptAllFileFilterUsed(false);

        // Instance to save
        OpenAndSaveFile save = new OpenAndSaveFile();

        if (saveFile.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {

            String filename;
            if(saveFile.getSelectedFile().getPath().contains(".dsd")){
                filename = saveFile.getSelectedFile().getPath();
            }
            else{
                filename = saveFile.getSelectedFile().getPath() + ".dsd";
            }// end if
            

            //Sequence information to save
            String sequenceInfo = "";

            sequenceInfo = sequenceInfo.concat(numberOfBitsComboBox.getSelectedItem().toString()).substring(0, 1).concat("-");

            sequenceInfo = sequenceInfo.concat(inputComboBox.getSelectedItem().toString()).concat("-");

            for (JTextField sequenceField : sequenceFields) {
                sequenceInfo = sequenceInfo.concat(sequenceField.getText()).concat("-");
            }// end for

            sequenceInfo = sequenceInfo.concat(cycleComboBox.getSelectedItem().toString()).concat("-");

            sequenceInfo = sequenceInfo.concat(String.valueOf(optionComboBox.getSelectedIndex()).concat("-"));

            if (optionComboBox.getSelectedIndex() != 0) {
                sequenceInfo = sequenceInfo.concat(optionFieldInput.getText());
            }// end if

            // save informations into the file
            save.write(filename, sequenceInfo);
        }// end if
    }// end method toBeSaved

    // Checks empty fields
    private boolean isFieldsEmpty() {
        // reset Values
        error.clear();
        errLocation = "";

        checkloop:

        // Locate error fields
        for (int getTextCount = 0; getTextCount < storeInput.numberOfInputs; getTextCount++) {
            if (sequenceFields[getTextCount].getText().isEmpty()) {
                error.add(getTextCount + 1);

            }// end if
        }// end for

        return !error.isEmpty();
    }// end method isFieldsEmpty

    // Check integers 
    private boolean isValueInteger() {
        // reset values
        error.clear();
        errLocation = "";

        // Locate error fields
        for (int getTextCount = 0; getTextCount < storeInput.numberOfInputs; getTextCount++) {
            if (!isInteger(sequenceFields[getTextCount].getText())) {
                error.add(getTextCount + 1);
            }// end if
        }// end for

        return error.isEmpty();
    }// end method isFieldContainingInteger

    // Check Range
    private boolean isValueOutOfRange() {

        // Reset Values
        error.clear();
        errLocation = "";

        // Locate error fields
        for (int getTextCount = 0; getTextCount < storeInput.numberOfInputs; getTextCount++) {
            if (Integer.parseInt(sequenceFields[getTextCount].getText()) > maxValue) {
                error.add(getTextCount + 1);
            }// end if
        }// end for 

        return !error.isEmpty();
    }// end method isValueOutOfRange

    // Check Dupicate
    private boolean isSequenceDuplicated() {

        // reset Values
        error.clear();
        errLocation = "";

        // Locate error duplicate fields
        outerLoop:
        for (int outerCount = 0; outerCount < storeInput.numberOfInputs; outerCount++) {

            innerLoop:
            for (int innerCount = 0; innerCount < storeInput.numberOfInputs; innerCount++) {

                outerIf:
                if (outerCount != innerCount) {

                    innerIf:
                    if (sequenceFields[outerCount].getText().equals(sequenceFields[innerCount].getText())) {
                        error.add(outerCount + 1);
                        error.add(innerCount + 1);
                        break outerLoop;
                    }// end inner if
                }// end outer if 
            }// end inner for
        }// end outer for

        return !error.isEmpty();
    }// end method isSequenceDuplicated

    // Check if a counter was selected
    private boolean isCounterSelected() {

        return cycleComboBox.getSelectedIndex() != 0;
    }// end method isCounterSelected

    // Check if a number of bits is selected
    private boolean isNumberOfBitsSelected() {
        return numberOfBitsComboBox.getSelectedIndex() != 0;
    }// end method isNumberOfBitsSelected

    // in case of starter option, it will validate inputs
    private boolean isStarter() {

        if (optionFieldInput.getText().isEmpty()) {

            // display error message
            Toolkit.getDefaultToolkit().beep();
            storeInput.displayMessage("Starter Number is Empty, Please enter value",
                    "EMPTY FIELD ERROR !!!");

            optionFieldInput.requestFocus();
        }// end if
        else if (!isInteger(optionFieldInput.getText())) {

            // display error message
            Toolkit.getDefaultToolkit().beep();
            storeInput.displayMessage("Did not enter correct input by starter Number Field,"
                    + " Please Check your Sequence", "INPUT ERROR !!!");

            optionFieldInput.setText("");
            optionFieldInput.requestFocus();
        }// end if
        else if (Integer.parseInt(optionFieldInput.getText()) > maxValue) {

            // display error message
            Toolkit.getDefaultToolkit().beep();
            storeInput.displayMessage("Starter number value is out of range,"
                    + " please choose between (0 - " + maxValue + ")", "RANGE ERROR !!!");

            optionFieldInput.requestFocus();
        }// end if
        else {
            for (int i = 0; i < Integer.parseInt((String) inputComboBox.getSelectedItem()); i++) {
                if (optionFieldInput.getText().equals(sequenceFields[i].getText())) {

                    error.add(i + 1);
                }// end if
            }// end for

            // Handling errors
            if (error.isEmpty()) { // if no error found return true
                return true;
            }// end  if
            else if (error.size() == 1) { // if there is only one error

                // display error message 
                Toolkit.getDefaultToolkit().beep();
                storeInput.displayMessage("Can not have duplicate numbers, Starter Field have same input with Field "
                        + error.get(0) + " of your sequence", "STARTER NUMBER DUPLICATE !!!");
                optionFieldInput.setText("");

            }// end if
            else if (error.size() > 1) { // if there is more than one field empty

                //Prepare locations of field errors
                for (Integer error1 : error) {
                    errLocation += error1 + ", ";
                } // end for

                // display error message 
                Toolkit.getDefaultToolkit().beep();
                storeInput.displayMessage("Can not have duplicate numbers, Starter Field have same input with Fields " + errLocation.substring(0, errLocation.length() - 2)
                        + " of your sequence", "STARTER NUMBER DUPLICATE !!!");

            }// end if 
            optionFieldInput.setText("");
            optionFieldInput.requestFocus();
        }// end IF
        return false;
    }// end method isStarter

    // in case of Illegal State option, it will validate inputs
    private boolean isIllegalState() {

        if (optionFieldInput.getText().isEmpty()) {

            // display error message
            Toolkit.getDefaultToolkit().beep();
            storeInput.displayMessage("Illegal State Number is Empty, Please enter value", "EMPTY FIELD ERROR !!!");

            optionFieldInput.requestFocus();
        }// end if
        else if (!isInteger(optionFieldInput.getText())) {

            // display error message
            Toolkit.getDefaultToolkit().beep();
            storeInput.displayMessage("Illegal State should be a number, please correct value", "ILLEGAL STATE INPUT ERROR !!!");

            optionFieldInput.setText("");
            optionFieldInput.requestFocus();
        }// end if
        else if (Integer.parseInt(optionFieldInput.getText()) > maxValue) {

            // display error message
            Toolkit.getDefaultToolkit().beep();
            storeInput.displayMessage("Illegal State number value is out of range,"
                    + " please choose between (0 - " + maxValue + ")", "RANGE ERROR !!!");
            optionFieldInput.setText("");
            optionFieldInput.requestFocus();
        }// end if
        else {
            for (int i = 0; i < sequenceFields.length; i++) {
                if (!optionFieldInput.getText().equals(sequenceFields[i].getText())) {
                    error.add(i);

                } else {
                    return true;
                }// end if
            }// end for 

            if (!error.isEmpty()) {

                // display error message 
                Toolkit.getDefaultToolkit().beep();
                storeInput.displayMessage("Illegal State Number does not match any of your Sequence Number", "ILLEGAL STATE INCORRECT !!!");
                optionFieldInput.setText("");
                optionFieldInput.requestFocus();
            }// end if
        }// end if

        return false;
    }// end method isIllegalState

    // Check if input is an integer
    private boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }// end catch

        return true;
    }// end method isInteger

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        logoPanel = new javax.swing.JPanel();
        logoLabel = new javax.swing.JLabel();
        buttonPanel = new javax.swing.JPanel();
        validateButton = new javax.swing.JButton();
        inputPanel = new javax.swing.JPanel();
        numberOfInputLabel = new javax.swing.JLabel();
        inputComboBox = new javax.swing.JComboBox();
        sequenceNumberLabel = new javax.swing.JLabel();
        sequenceInputPanel = new javax.swing.JPanel();
        sequenceInputLabel = new javax.swing.JLabel();
        typeOfCounterLabel = new javax.swing.JLabel();
        cycleComboBox = new javax.swing.JComboBox();
        starterInputPanel = new javax.swing.JPanel();
        illegalStateLabel = new javax.swing.JLabel();
        optionComboBox = new javax.swing.JComboBox();
        optionFieldPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        numberOfBitsComboBox = new javax.swing.JComboBox();
        DSDMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        openFileMenuItem = new javax.swing.JMenuItem();
        saveFileMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        userManualMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("DigitalSD");

        logoPanel.setBackground(new java.awt.Color(255, 255, 255));
        logoPanel.setOpaque(false);
        logoPanel.setLayout(new java.awt.GridBagLayout());

        logoLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        logoLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/DigitalSD.png"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        logoPanel.add(logoLabel, gridBagConstraints);

        getContentPane().add(logoPanel, java.awt.BorderLayout.PAGE_START);

        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 10));

        validateButton.setBackground(new java.awt.Color(204, 204, 204));
        validateButton.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        validateButton.setMnemonic('V');
        validateButton.setText("Validate");
        validateButton.setToolTipText("Validate inputs");
        validateButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        validateButton.setEnabled(false);
        validateButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                validateButtonMouseClicked(evt);
            }
        });
        validateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validateButtonActionPerformed(evt);
            }
        });
        validateButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                validateButtonKeyPressed(evt);
            }
        });
        buttonPanel.add(validateButton);

        getContentPane().add(buttonPanel, java.awt.BorderLayout.PAGE_END);

        inputPanel.setBackground(new java.awt.Color(255, 255, 255));
        inputPanel.setOpaque(false);
        inputPanel.setLayout(new java.awt.GridBagLayout());

        numberOfInputLabel.setFont(new java.awt.Font("Cambria", 1, 16)); // NOI18N
        numberOfInputLabel.setText("Number of inputs :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 10);
        inputPanel.add(numberOfInputLabel, gridBagConstraints);

        ((JLabel)inputComboBox.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        inputComboBox.setBackground(new java.awt.Color(51, 51, 51));
        inputComboBox.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        inputComboBox.setForeground(new java.awt.Color(204, 204, 204));
        inputComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Inputs", "4", "5", "6", "7" }));
        inputComboBox.setToolTipText("Select the numbers of inputs for your sequence");
        inputComboBox.setEnabled(false);
        inputComboBox.setOpaque(false);
        inputComboBox.setPreferredSize(new java.awt.Dimension(100, 30));
        inputComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                inputComboBoxMouseClicked(evt);
            }
        });
        inputComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputComboBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 10);
        inputPanel.add(inputComboBox, gridBagConstraints);

        sequenceNumberLabel.setFont(new java.awt.Font("Cambria", 1, 16)); // NOI18N
        sequenceNumberLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sequenceNumberLabel.setText("Sequence number :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        inputPanel.add(sequenceNumberLabel, gridBagConstraints);

        sequenceInputPanel.setBackground(new java.awt.Color(255, 255, 255));
        sequenceInputPanel.setOpaque(false);

        sequenceInputLabel.setFont(new java.awt.Font("Cambria", 1, 15)); // NOI18N
        sequenceInputLabel.setForeground(new java.awt.Color(255, 51, 51));
        sequenceInputLabel.setText("Select 'Number of Inputs' first !!!");
        sequenceInputLabel.setToolTipText("After Choosing numbers of inputs, enter Sequence here");
        sequenceInputPanel.add(sequenceInputLabel);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 2.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        inputPanel.add(sequenceInputPanel, gridBagConstraints);

        typeOfCounterLabel.setFont(new java.awt.Font("Cambria", 1, 16)); // NOI18N
        typeOfCounterLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        typeOfCounterLabel.setText("Type of counter :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        inputPanel.add(typeOfCounterLabel, gridBagConstraints);

        ((JLabel)cycleComboBox.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        cycleComboBox.setBackground(new java.awt.Color(51, 51, 51));
        cycleComboBox.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        cycleComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select counter type", "Down Counter", "Up Counter", "Up/Down Counter" }));
        cycleComboBox.setToolTipText("Select type of Counter");
        cycleComboBox.setEnabled(false);
        cycleComboBox.setOpaque(false);
        cycleComboBox.setPreferredSize(new java.awt.Dimension(180, 30));
        cycleComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cycleComboBoxMouseClicked(evt);
            }
        });
        cycleComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cycleComboBoxActionPerformed(evt);
            }
        });
        cycleComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cycleComboBoxKeyPressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        inputPanel.add(cycleComboBox, gridBagConstraints);

        starterInputPanel.setOpaque(false);
        starterInputPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 0, 0);
        inputPanel.add(starterInputPanel, gridBagConstraints);

        illegalStateLabel.setFont(new java.awt.Font("Cambria", 1, 16)); // NOI18N
        illegalStateLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        illegalStateLabel.setText("Sequence option :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        inputPanel.add(illegalStateLabel, gridBagConstraints);

        ((JLabel)optionComboBox.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        optionComboBox.setBackground(new java.awt.Color(51, 51, 51));
        optionComboBox.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        optionComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Sequence only", "Start sequence At", "Illegal state revert To" }));
        optionComboBox.setToolTipText("Select option of your sequence");
        optionComboBox.setEnabled(false);
        optionComboBox.setOpaque(false);
        optionComboBox.setPreferredSize(new java.awt.Dimension(180, 30));
        optionComboBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                optionComboBoxMouseClicked(evt);
            }
        });
        optionComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optionComboBoxActionPerformed(evt);
            }
        });
        optionComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                optionComboBoxKeyPressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        inputPanel.add(optionComboBox, gridBagConstraints);

        optionFieldPanel.setPreferredSize(new java.awt.Dimension(40, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.insets = new java.awt.Insets(7, 0, 0, 0);
        inputPanel.add(optionFieldPanel, gridBagConstraints);

        jLabel1.setFont(new java.awt.Font("Cambria", 1, 16)); // NOI18N
        jLabel1.setText("Number of bits :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 10);
        inputPanel.add(jLabel1, gridBagConstraints);

        ((JLabel)numberOfBitsComboBox.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        numberOfBitsComboBox.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        numberOfBitsComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Bits", "3 Bits", "4 Bits" }));
        numberOfBitsComboBox.setToolTipText("Select number of bits");
        numberOfBitsComboBox.setPreferredSize(new java.awt.Dimension(100, 30));
        numberOfBitsComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numberOfBitsComboBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 10);
        inputPanel.add(numberOfBitsComboBox, gridBagConstraints);

        getContentPane().add(inputPanel, java.awt.BorderLayout.CENTER);

        DSDMenuBar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        DSDMenuBar.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        DSDMenuBar.setOpaque(false);

        fileMenu.setMnemonic('F');
        fileMenu.setText("File");
        fileMenu.setFont(new java.awt.Font("Cambria", 1, 16)); // NOI18N

        openFileMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        openFileMenuItem.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        openFileMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/openIcon.png"))); // NOI18N
        openFileMenuItem.setMnemonic('O');
        openFileMenuItem.setText("Open Sequence");
        openFileMenuItem.setToolTipText("Open saved sequence ");
        openFileMenuItem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        openFileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openFileMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openFileMenuItem);

        saveFileMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        saveFileMenuItem.setFont(openFileMenuItem.getFont());
        saveFileMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/save_icon.png"))); // NOI18N
        saveFileMenuItem.setMnemonic('S');
        saveFileMenuItem.setText("Save Sequence");
        saveFileMenuItem.setToolTipText("Save your sequence");
        saveFileMenuItem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        saveFileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveFileMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveFileMenuItem);

        DSDMenuBar.add(fileMenu);

        helpMenu.setMnemonic('H');
        helpMenu.setText("Help");
        helpMenu.setFont(fileMenu.getFont());

        userManualMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        userManualMenuItem.setFont(openFileMenuItem.getFont());
        userManualMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/Help Icon.png"))); // NOI18N
        userManualMenuItem.setMnemonic('U');
        userManualMenuItem.setText("User Manual");
        userManualMenuItem.setToolTipText("User Manual of DigitalSD");
        userManualMenuItem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        userManualMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userManualMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(userManualMenuItem);

        aboutMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F2, 0));
        aboutMenuItem.setFont(openFileMenuItem.getFont());
        aboutMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/About Icon.png"))); // NOI18N
        aboutMenuItem.setMnemonic('A');
        aboutMenuItem.setText("About DSD");
        aboutMenuItem.setToolTipText("About DigitalSD");
        aboutMenuItem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        DSDMenuBar.add(helpMenu);

        setJMenuBar(DSDMenuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sequenceFieldsMouseReleased(MouseEvent e) {
        currentField = getPosition();
        
        sequenceFields[currentField].selectAll();

    }// end method sequenceFieldsMouseReleased

    // Get the current position of the cursor in the sequence field
    private int getPosition() {
        
        for (int i = 0; i < sequenceFields.length; i++) {
            if (sequenceFields[i].isFocusOwner()) {
                return i;
            }// end if
        }// end for

        return 0;
    }// end method getPosition

    private void sequenceFieldsKeyPressed(KeyEvent e) {
        currentField = getPosition();
        int maxFieldCharacter;

        if (numberOfBitsComboBox.getSelectedIndex() != 1) {
            maxFieldCharacter = 2;
        } else {
            maxFieldCharacter = 1;
        }// end if

        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        if (e.getKeyCode() != KeyEvent.VK_CAPS_LOCK && e.getKeyCode() != KeyEvent.VK_BACK_SPACE
                && sequenceFields[currentField].getText().length() == maxFieldCharacter) {

            if (!isInteger(sequenceFields[currentField].getText())){ // if not is an integer, clear field
                sequenceFields[currentField].setText("");
                Toolkit.getDefaultToolkit().beep();
                sequenceFields[currentField].requestFocus();
                
            } else {
                if(Integer.parseInt(sequenceFields[currentField].getText()) > maxValue){ // if out of range, clear fill 
                    sequenceFields[currentField].setText("");
                    Toolkit.getDefaultToolkit().beep();
                    
                } else { // else move to next field
                    manager.focusNextComponent();
                    sequenceFields[currentField].selectAll();
                }// end if
            }// end if
        }// end if 
    }// end method sequenceFieldsKeyPressed

    private void optionFieldInputKeyPressed(KeyEvent evt) {

        // validate input if Enter key is pressed on the keyboard
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            // Valid input in case using keyboard
            captureInputs();
        }// end if
    }   // end method optionFieldInputKeyPressed

    private void openFileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openFileMenuItemActionPerformed

        Thread openFileThread;
        openFileThread = new Thread() {
            @Override
            public void run() {
                if (storeInput.isDirectory()) {

                    // Open JFilechoose to default folder
                    JFileChooser openFile = new JFileChooser(storeInput.dsdDir);

                    // Personalised JFileChooser
                    FileNameExtensionFilter typeFormat = new FileNameExtensionFilter("Digital Sequence Format", "dsd");
                    openFile.setFileFilter(typeFormat);
                    openFile.setAcceptAllFileFilterUsed(false);

                    // Read File
                    OpenAndSaveFile open = new OpenAndSaveFile();

                    if (openFile.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                        try {
                            String[] sequenceInformation;

                            int getSequenceCount;
                            //read file from path
                            open.readFile(openFile.getSelectedFile().getPath());

                            // split sonformation by "-" because sequence infor are saved separated by dash
                            sequenceInformation = open.getFileContent().split("-");

                            // Get number of bits
                            storeInput.numberOfBits = Integer.parseInt(sequenceInformation[0]);
                            
                            // get number of inputs
                            storeInput.numberOfInputs = Integer.parseInt(sequenceInformation[1]);
                            
                            // Get sequence number
                            for (getSequenceCount = 2; getSequenceCount < storeInput.numberOfInputs + 2; getSequenceCount++) {
                                storeInput.decimalSequence.add(sequenceInformation[getSequenceCount]);
                            }// end for
                            
                            // cycle of the sequence
                            storeInput.cycle = sequenceInformation[storeInput.numberOfInputs + 2];
                            
                            // Option choosen by the user
                            storeInput.optionChoosen = Integer.parseInt(sequenceInformation[storeInput.numberOfInputs + 3]);
                            
                            // get Option Field value
                            storeInput.sequenceOptionField = sequenceInformation[storeInput.numberOfInputs + 4].substring(0, 1);
                            
                            // output informations in the outputs GUI
                            outputGUI();
                        } catch (Exception e) {
                            // clear arrayList
                            storeInput.binarySequence.clear();
                            storeInput.decimalSequence.clear();
                            storeInput.displayMessage("Oups File is corrupted", "Could not Open !!!");

                        }// end catch
                    } // End if
                }// end if
            }// end method run
        };
        openFileThread.start();
    }//GEN-LAST:event_openFileMenuItemActionPerformed

    private void saveFileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveFileMenuItemActionPerformed

        // Save File
        if (storeInput.isDirectory()) {
            if (numberOfBitsComboBox.getSelectedIndex() != 0) {
                if (inputComboBox.getSelectedIndex() != 0) {// index of comboBox different from 0 go to the next check 
                    if (!isFieldsEmpty()) { // check if any field is empty
                        if (isValueInteger()) { // check if valie is integer
                            if (!isValueOutOfRange()) { // check if valie is out of range
                                if (!isSequenceDuplicated()) { // check if sequence contqins duplicate number
                                    if (isCounterSelected()) {// check a type of counter has been selected
                                        if (optionComboBox.getSelectedIndex() == 0) {// if no starter is Selected
                                            toBeSaved();

                                        }// end if
                                        else if (optionComboBox.getSelectedIndex() == 1) {// if Start Sequence at is selected
                                            if (isStarter()) {
                                                toBeSaved();

                                            }// end if check starter
                                        }// end if
                                        else if (optionComboBox.getSelectedIndex() == 2) { // if illegal sate option is selected
                                            if (isIllegalState()) {
                                                toBeSaved();

                                            }// end if
                                        }// end if
                                    } else {

                                        // 
                                        Toolkit.getDefaultToolkit().beep();
                                        storeInput.displayMessage("Please Select a type of counter before saving", "SAVING INPUTS ERROR !!!");
                                    }// end id isCounterSelected
                                } else {
                                    if (error.size() == 2) { // display error for the first two Same fields

                                        // display error message 
                                        Toolkit.getDefaultToolkit().beep();
                                        storeInput.displayMessage("Sequence Field " + error.get(0) + ", " + error.get(1)
                                                + " are equals, Can not save Sequence with duplicate numbers", "SAVING INPUTS ERROR !!!");

                                        // Set Cursor to field error
                                        sequenceFields[error.get(0)].setText("");
                                        sequenceFields[error.get(0)].requestFocus();
                                    }//end if 
                                }// end if isSequenceDuplicated

                            } else {
                                if (error.size() == 1) { 

                                    // display error message 
                                    Toolkit.getDefaultToolkit().beep();
                                    storeInput.displayMessage("Sequence Field " + error.get(0) + " is out of range. Can not save it, please choose values between (0 - 7)", "SAVING INPUTS ERROR !!!");

                                    // Set Cursor to field error
                                    sequenceFields[error.get(0) - 1].requestFocus();
                                }// end if
                                else if (error.size() > 1) { 

                                    //Prepare locations of field errors
                                    for (Integer error1 : error) {
                                        errLocation += error1 + ", ";
                                    } // end for

                                    // display error message 
                                    Toolkit.getDefaultToolkit().beep();
                                    storeInput.displayMessage("Sequence Fields " + errLocation.substring(0, errLocation.length() - 2) + " are out of range. Can not save them, please choose values between (0 - 7)", "SAVING INPUTS ERROR !!!");

                                    // Set Cursor to first field error
                                    sequenceFields[error.get(0) - 1].requestFocus();
                                }// end if
                            }// end if isValueOutRange

                        } else {
                            if (error.size() == 1) { 

                                // display error message 
                                Toolkit.getDefaultToolkit().beep();
                                storeInput.displayMessage("Can not save wrong input located at Sequence field " + error.get(0)
                                        + ", Please Check your Sequence", "SAVING INPUTS ERROR !!!");

                                sequenceFields[error.get(0) - 1].setText("");
                                // Set Cursor to field error
                                sequenceFields[error.get(0) - 1].requestFocus();
                            }// end if 
                            else if (error.size() > 1) { 

                                //Prepare locations of field errors
                                for (Integer error1 : error) {
                                    errLocation += error1 + ", ";
                                }// end for

                                // display error message 
                                Toolkit.getDefaultToolkit().beep();
                                storeInput.displayMessage("Can not save wrong inputs located at Sequence fields " + errLocation.substring(0, errLocation.length() - 2)
                                        + ", Please Check your Sequence", "SAVING INPUTS ERROR !!!");

                                // Set Cursor to first field error
                                for (Integer error1 : error) {
                                    sequenceFields[error1 - 1].setText("");
                                }
                                sequenceFields[error.get(0) - 1].requestFocus();
                            }// end if
                        }// end if isFieldContainingInteger

                    } else {

                        if (error.size() == 1) { 

                            // display error message 
                            Toolkit.getDefaultToolkit().beep();
                            storeInput.displayMessage("Can not save empty fields, Sequence Input Field " + error.get(0) + " is Empty, Please enter value",
                                    "SAVING INPUTS ERROR !!!");

                            // Set Cursor to field error
                            sequenceFields[error.get(0) - 1].requestFocus();

                        }// end if
                        else if (error.size() > 1) { 

                            //Prepare locations of field errors
                            for (Integer error1 : error) {
                                errLocation += error1 + ", ";
                            } // end for

                            // display error message
                            Toolkit.getDefaultToolkit().beep();
                            storeInput.displayMessage("Can not save empty fields, Sequence Input Fields " + errLocation.substring(0, errLocation.length() - 2) + " are Empty, Please enter values",
                                    "SAVING INPUTS ERROR !!!");

                            // Set Cursor to first field error
                            sequenceFields[error.get(0) - 1].requestFocus();

                        }// end if

                    }// end if isFieldEmpty
                } else {

                    // display error message
                    Toolkit.getDefaultToolkit().beep();
                    storeInput.displayMessage("You have not entered any inputs yet, please enter inputs", "SAVING INPUTS ERROR !!!");

                    inputComboBox.requestFocus();
                }// end if check of input comboBox
            } else {

                // display error message
                Toolkit.getDefaultToolkit().beep();
                storeInput.displayMessage("You have not select the number of Bits yet, please enter Select number of Bits", "SAVING INPUTS ERROR !!!");

                numberOfBitsComboBox.requestFocus();
            }// end if check number of bits comboBox
        }// end if isDirectory
    }//GEN-LAST:event_saveFileMenuItemActionPerformed

    private void userManualMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userManualMenuItemActionPerformed

        // User Manual Menu Item ActionListener
        UserManualForm helpScreen = new UserManualForm();
        helpScreen.setLocationRelativeTo( null );
        helpScreen.setVisible( true );
    }//GEN-LAST:event_userManualMenuItemActionPerformed

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed

        // About Menu Item ActionListener
        AboutDSDForm aboutDSD = new AboutDSDForm();
        aboutDSD.setVisible(true);
        aboutDSD.setLocationRelativeTo(null);
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void validateButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_validateButtonMouseClicked
        if (!validateButton.isEnabled()) {
            Toolkit.getDefaultToolkit().beep();
        }// end if
    }//GEN-LAST:event_validateButtonMouseClicked

    private void validateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_validateButtonActionPerformed
        captureInputs();
    }//GEN-LAST:event_validateButtonActionPerformed

    private void validateButtonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_validateButtonKeyPressed
        captureInputs();
    }//GEN-LAST:event_validateButtonKeyPressed

    private void inputComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputComboBoxActionPerformed

        cycleComboBox.setEnabled(true);
        if (inputComboBox.getSelectedIndex() == 0) {// For first value of the comboBox

            // Reset Sequence inputs areas
            sequenceInputPanel.removeAll();
            sequenceInputLabel = new JLabel("Select 'Number of Inputs' first !!!");
            sequenceInputLabel.setForeground(new Color(255, 51, 51));
            sequenceInputLabel.setFont(new Font("Cambria", Font.BOLD, 15));
            sequenceInputPanel.add(sequenceInputLabel);
            sequenceInputPanel.validate();
            sequenceInputPanel.repaint();

            // Reset cycle
            cycleComboBox.setSelectedIndex(0);
            cycleComboBox.setEnabled(false);
            optionComboBox.setEnabled(false);
            optionComboBox.setSelectedIndex(0);
            cycleComboBox.setSelectedIndex(0);
            cycleComboBox.setEnabled(false);
            optionComboBox.setSelectedIndex(0);
            optionComboBox.setEnabled(false);

            // reset option input
            inputComboBox.setBackground(new Color(51, 51, 51));
            inputComboBox.setForeground(new Color(204, 204, 204));
            optionComboBox.setBackground(new Color(51, 51, 51));
            optionComboBox.setSelectedIndex(0);

            // Make sure the option Field input is not added
            inputPanel.remove(optionFieldInput);
            inputPanel.validate();
            inputPanel.repaint();

        } else {

            sequenceInputPanel.removeAll();
            if (inputComboBox.getSelectedIndex() == 1) { // Counter of 4 Numbers

                // Generate Field Sequence inputs according to the selection
                storeInput.numberOfInputs = Integer.parseInt((String) inputComboBox.getSelectedItem());
                FieldInput(storeInput.numberOfInputs);

            } else if (inputComboBox.getSelectedIndex() == 2) { // Counter of 5 Numbers

                // Generate Field Sequence inputs according to the selection
                storeInput.numberOfInputs = Integer.parseInt((String) inputComboBox.getSelectedItem());
                FieldInput(storeInput.numberOfInputs);

            } else if (inputComboBox.getSelectedIndex() == 3) { // Counter of 6 Numbers

                // Generate Field Sequence inputs according to the selection
                storeInput.numberOfInputs = Integer.parseInt((String) inputComboBox.getSelectedItem());
                FieldInput(storeInput.numberOfInputs);

            } else if (inputComboBox.getSelectedIndex() == 4) { // Counter of 7 Numbers

                storeInput.numberOfInputs = Integer.parseInt((String) inputComboBox.getSelectedItem());
                FieldInput(storeInput.numberOfInputs);

            }// end if

            // set Background and foreground color of the comboBox
            inputComboBox.setBackground(new Color(255, 51, 51));
            inputComboBox.setForeground(new Color(0, 0, 0));

            // Disable cycle
            cycleComboBox.setSelectedIndex(0);
        }// end if

        sequenceInputPanel.validate();
        sequenceInputPanel.repaint();
    }//GEN-LAST:event_inputComboBoxActionPerformed

    private void cycleComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cycleComboBoxMouseClicked
        if (!cycleComboBox.isEnabled()) {
            Toolkit.getDefaultToolkit().beep();
        }// end if
    }//GEN-LAST:event_cycleComboBoxMouseClicked

    private void cycleComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cycleComboBoxActionPerformed

        // If index of comboBox is deiiferent from the first index
        if (cycleComboBox.getSelectedIndex() != 0) {

            // set Background and foreground color of the comboBox
            cycleComboBox.setBackground(new Color(255, 51, 51));
            cycleComboBox.setForeground(new Color(0, 0, 0));

            // Enable the option comboBox
            optionComboBox.setEnabled(true);
            optionComboBox.requestFocus();
            optionComboBox.setSelectedIndex(0);

            // set Background and foreground color of the comboBox
            optionComboBox.setBackground(new Color(255, 51, 51));
            optionComboBox.setForeground(new Color(0, 0, 0));

            // enabled validate button
            validateButton.setEnabled(true);

        } else { // else if equals to 0

            // disable option comboBox
            optionComboBox.setEnabled(false);
            optionComboBox.setSelectedIndex(0);
            optionComboBox.setBackground(new Color(51, 51, 51));

            // set Background and foreground color of the comboBox
            cycleComboBox.setBackground(new Color(51, 51, 51));
            cycleComboBox.setForeground(new Color(204, 204, 204));

            //Remove component from panel
            inputPanel.remove(optionFieldInput);
            inputPanel.validate();
            inputPanel.repaint();

            // diable valide button
            validateButton.setEnabled(false);
        }// end if
    }//GEN-LAST:event_cycleComboBoxActionPerformed

    private void cycleComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cycleComboBoxKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (cycleComboBox.getSelectedIndex() == 0) {

                Toolkit.getDefaultToolkit().beep();
                storeInput.displayMessage("Select type of counter first...", "Cycle not Selected !!!");
            }// end inner if
        }// end outer if
    }//GEN-LAST:event_cycleComboBoxKeyPressed

    private void optionComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_optionComboBoxMouseClicked
        if (!optionComboBox.isEnabled()) {
            Toolkit.getDefaultToolkit().beep();
        }// end if
    }//GEN-LAST:event_optionComboBoxMouseClicked

    private void optionComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optionComboBoxActionPerformed

        // if index of comboBox is different from 0
        if (optionComboBox.getSelectedIndex() != 0) {

            // set Background and foreground color of the comboBox
            optionComboBox.setBackground(new Color(255, 51, 51));
            optionComboBox.setForeground(new Color(0, 0, 0));

            // display Input field
            counterOption();

        } else {//else index of comobBox is 0

            // remove option input field if there is one
            optionFieldPanel.remove(optionFieldInput);
            optionFieldPanel.validate();

        }// end if
    }//GEN-LAST:event_optionComboBoxActionPerformed

    private void optionComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_optionComboBoxKeyPressed

        // validate input if Enter key is pressed on the keyboard
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (optionComboBox.getSelectedIndex() == 0) {

                captureInputs();
            }// end inner if
        }// end outer if

    }//GEN-LAST:event_optionComboBoxKeyPressed

    private void numberOfBitsComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numberOfBitsComboBoxActionPerformed

        inputComboBox.setEnabled(true); // enable comboBox
        
        // number of bits not selected
        if (numberOfBitsComboBox.getSelectedIndex() != 0) {
            
            // Reset inputs components
            numberOfBitsComboBox.setBackground(new Color(255, 51, 51));
            numberOfBitsComboBox.setForeground(new Color(0, 0, 0));
            
            cycleComboBox.setSelectedIndex(0);
            cycleComboBox.setEnabled(false);
            
            optionComboBox.setEnabled(false);
            optionComboBox.setSelectedIndex(0);
            
            cycleComboBox.setSelectedIndex(0);
            cycleComboBox.setEnabled(false);
            
            optionComboBox.setSelectedIndex(0);
            optionComboBox.setEnabled(false);
            
            inputComboBox.requestFocus();
            inputComboBox.setSelectedIndex(0);

        } else { // else select number of bits
            
            numberOfBitsComboBox.setBackground(new Color(51, 51, 51));
            numberOfBitsComboBox.setForeground(new Color(204, 204, 204));
            
            inputComboBox.setEnabled(false);
            inputComboBox.setSelectedIndex(0);
            inputComboBox.setBackground(new Color(51, 51, 51));
            inputComboBox.setForeground(new Color(204, 204, 204));

            cycleComboBox.setEnabled(false);
            cycleComboBox.setSelectedIndex(0);
            cycleComboBox.setBackground(new Color(51, 51, 51));
            cycleComboBox.setForeground(new Color(204, 204, 204));

            optionComboBox.setEnabled(false);
            optionComboBox.setSelectedIndex(0);
            optionComboBox.setBackground(new Color(51, 51, 51));
            optionComboBox.setForeground(new Color(204, 204, 204));

        }// end if
    }//GEN-LAST:event_numberOfBitsComboBoxActionPerformed

    private void inputComboBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_inputComboBoxMouseClicked
        if (!inputComboBox.isEnabled()) {
            Toolkit.getDefaultToolkit().beep();
        }// end if
    }//GEN-LAST:event_inputComboBoxMouseClicked

    // Set Icon
    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/digitalsequencedesigner/ImagesDSD/Logo Icon.png")));
    }// end method setIcon    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar DSDMenuBar;
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JComboBox cycleComboBox;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JLabel illegalStateLabel;
    private javax.swing.JComboBox inputComboBox;
    private javax.swing.JPanel inputPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel logoLabel;
    private javax.swing.JPanel logoPanel;
    private javax.swing.JComboBox numberOfBitsComboBox;
    private javax.swing.JLabel numberOfInputLabel;
    private javax.swing.JMenuItem openFileMenuItem;
    private javax.swing.JComboBox optionComboBox;
    private javax.swing.JPanel optionFieldPanel;
    private javax.swing.JMenuItem saveFileMenuItem;
    private javax.swing.JLabel sequenceInputLabel;
    private javax.swing.JPanel sequenceInputPanel;
    private javax.swing.JLabel sequenceNumberLabel;
    private javax.swing.JPanel starterInputPanel;
    private javax.swing.JLabel typeOfCounterLabel;
    private javax.swing.JMenuItem userManualMenuItem;
    private javax.swing.JButton validateButton;
    // End of variables declaration//GEN-END:variables
}
