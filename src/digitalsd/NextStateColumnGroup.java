package digitalsd;

/**
 * NextStateColumnGroup
 * 
 * From http://www.crionics/com/products/opensource/faq/swing_ex/ swingExamples.html
 * found on www.java2s.com/Code/java/Swing-Component/ GroupHeaderExample.htm
 * 
 * 
 * Modified by: Nobuo Tamemasa
 * @version 1.0 20.10.1998
 * @author Nobuo Tamemasa
 * 
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */

import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;


public class NextStateColumnGroup {

    // Instance Variable
    protected TableCellRenderer renderer;

    protected List<TableColumn> columns;
    protected List<NextStateColumnGroup> groups;

    protected String text;
    protected int margin = 0;
    
    // constructor
    public NextStateColumnGroup(String text) {
        this(text, null);
    }//e dn constructor

    
    public NextStateColumnGroup(String text, TableCellRenderer renderer) {
        this.text = text;
        this.renderer = renderer;
        this.columns = new ArrayList<>();
        this.groups = new ArrayList<>();
    }// end method NextStateColumnGroup

    protected void addColumn(TableColumn column) {
        columns.add(column);
    }// end method addColumn

    protected void addColumnGroup(NextStateColumnGroup group) {
        groups.add(group);
    }// end method addColumnGroup

    /**
     * @param column
     * TableColumn
     * @return 
     */
    protected List<NextStateColumnGroup> getColumnGroups(TableColumn column) {
        if (!contains(column)) {
            return Collections.emptyList();
        }// end if
        
        List<NextStateColumnGroup> result = new ArrayList<>();
        result.add(this);
        if (columns.contains(column)) {
            return result;
        }// end if
        for (NextStateColumnGroup columnGroup : groups) {
            result.addAll(columnGroup.getColumnGroups(column));
        }// end for
        
        return result;
    }// end method getColumnGroups

    protected boolean contains(TableColumn column) {
        if (columns.contains(column)) {
            return true;
        }// end if
        for (NextStateColumnGroup group : groups) {
            if (group.contains(column)) {
                return true;
            }// end if
        }// end for
        
        return false;
    }// end method contains

    protected TableCellRenderer getHeaderRenderer() {
        return renderer;
    }// end method getHeaderRenderer

    protected void setHeaderRenderer(TableCellRenderer renderer) {
        this.renderer = renderer;
    }// end method setHeaderRenderer

    protected String getHeaderValue() {
        return text;
    }// end method getHeaderValue

    protected Dimension getSize(JTable table) {
        TableCellRenderer tableRenderer = this.renderer;
        
        if (tableRenderer == null) {
            tableRenderer = table.getTableHeader().getDefaultRenderer();
        }// end if
        
        Component comp = tableRenderer.getTableCellRendererComponent(table, getHeaderValue() == null || getHeaderValue().trim().isEmpty() ? " "
                : getHeaderValue(), false, false, -1, -1);
        int height = comp.getPreferredSize().height;
        int width = 0;
        
        for (NextStateColumnGroup columnGroup : groups) {
            width += columnGroup.getSize(table).width;
        }// end for
        
        for (TableColumn tableColumn : columns) {
            width += tableColumn.getWidth();
            width += margin;
        }// end for
        
        return new Dimension(width, height);
    }// end method getSize

    protected void setColumnMargin(int margin) {
        this.margin = margin;
        for (NextStateColumnGroup columnGroup : groups) {
            columnGroup.setColumnMargin(margin);
        }// end for 
    }// end method setColumnMargin

}// end class NextStateColumnGroup
