
package digitalsd;

import java.util.ArrayList;

/**
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */

public class CoveredMinterms {
    
    // Instances Variable
    protected ArrayList <Integer> coveringNumbers;

   // Constructor
   public CoveredMinterms(ArrayList<Integer> covered2, ArrayList<Integer> covered1) {
       
       coveringNumbers = new ArrayList<>();
       
       // Populate arrayList with Data
       coveringNumbers.addAll(covered1);
       coveringNumbers.addAll(covered2);
       
   }// end constructor   
}// end class CoveredMinterms
