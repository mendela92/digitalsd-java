package digitalsd;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * JTable redimensionable
 * User: alberto
 * Date: 23/01/13
 * Time: 1:04
 * 
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */

public class KarnaughMaps extends JPanel{
    
    // Instances variables
    private final GetInput kMapInputs = GetInput.getInstance();
    protected JTable table = null;
    private final JScrollPane scrollPane;

    /**
     * @param outputs
     */
    public KarnaughMaps(ArrayList <String> outputs) {

        // defining the type of counter it will create a tabme model accordingly
        
        if(kMapInputs.cycle.equalsIgnoreCase("Up/Down Counter") || kMapInputs.numberOfBits == 4){// if up and down counter
            table = new JTable(kMapRowData() , new Object[]{"00", "01","11","10"});
            scrollPane = new JScrollPane(table);
            scrollPane.setOpaque(false);
            scrollPane.setPreferredSize(new Dimension(230, 182));
            
        } else { // else if down or up counter
            
            table = new JTable(kMapRowData(), new Object[]{"0", "1"});

            scrollPane = new JScrollPane(table);
            scrollPane.setPreferredSize(new Dimension(150, 182));
        }// end if
        
        table.setRowHeight(31);                                     // set height of the table
        table.setEnabled(false);                                    // disable the table selection 
        table.setFocusable(false);                                  // disable focus of the table
        table.getTableHeader().setReorderingAllowed(false);         // disable rearrangement of the columns
        table.getTableHeader().setResizingAllowed(false);           // disable table header resizability
        table.setFont(new Font("Cambria", 1, 18));                  // set font of the table
        
        // Center Values in Table
        DefaultTableCellRenderer centerKmapTables = new DefaultTableCellRenderer();
        centerKmapTables.setHorizontalAlignment( JLabel.CENTER );
        table.setDefaultRenderer(String.class, centerKmapTables);
        
        // Set Font the table header
        table.getTableHeader().setFont(new Font("Cambria", Font.ITALIC+ Font.BOLD, 17));

        // Center content of headers
        ((DefaultTableCellRenderer)table.getTableHeader().getDefaultRenderer())
            .setHorizontalAlignment(JLabel.CENTER);        
        
        // Set Border with Titles
        if(kMapInputs.numberOfBits == 3){
            if(kMapInputs.cycle.equalsIgnoreCase("Up/Down Counter")){
                scrollPane.setBorder(BorderFactory.createTitledBorder(null, kMapInputs.subHeaderInputsNames()[0]+ kMapInputs.subHeaderInputsNames()[1]+"/"+ kMapInputs.subHeaderInputsNames()[2]+ "Y",
                        1, 2, new Font("Cambria", Font.BOLD, 13)));
            } else {
                scrollPane.setBorder(BorderFactory.createTitledBorder(null, kMapInputs.subHeaderInputsNames()[0]+ kMapInputs.subHeaderInputsNames()[1]+"/"+ kMapInputs.subHeaderInputsNames()[2],
                        1, 2, new Font("Cambria", Font.BOLD, 13)));
            }// end if
            
        } else if(kMapInputs.numberOfBits == 4){
            scrollPane.setBorder(BorderFactory.createTitledBorder(kMapInputs.subHeaderInputsNames()[0]+ kMapInputs.subHeaderInputsNames()[1]+"/"+ kMapInputs.subHeaderInputsNames()[2]+ kMapInputs.subHeaderInputsNames()[3]));

        }// end if
        
        // add row header to the table
        scrollPane.setRowHeaderView(buildRowHeader(table)); 
        
        // add scrol
        add( scrollPane );
        
        // filling the karnaugh Maps
        fillKmap(outputs);
        
    }// end constructor
    
    // Generating kMap rows
    private Object[][] kMapRowData(){
        Object [][] kMapData = new Object [4][4];
        for (int rowCounter = 0; rowCounter < 4; rowCounter++) {
            if(kMapInputs.numberOfBits == 4 || kMapInputs.numberOfBits == 3 && kMapInputs.cycle.equals("Up/Down Counter") ){
                for (int j = 0; j < 4; j++) {
                    kMapData [rowCounter][j] = new Object();
                    kMapData [rowCounter][j] = "";

                }// end for
            } else{
                for (int columnCounter = 0; columnCounter < 2; columnCounter++) {
                    kMapData [rowCounter][columnCounter] = new Object();
                    kMapData [rowCounter][columnCounter] = "";

                }// end for
            }// end if
        }// end 
        return kMapData;
    }// end method kMapRowData
    
    // Filling the karnaugh Map tables
    private void fillKmap(ArrayList <String> out){
        
        // Local variables
        int [] indice = {0, 1, 3, 2};
        int counter = 0;
        
        // if up and down counter
        if(kMapInputs.cycle.equalsIgnoreCase("Up/Down Counter") || kMapInputs.numberOfBits == 4){
            rowLoop:
            for (int rowCounter = 0; rowCounter < 4; rowCounter++) {
                
                columnLoop:
                for (int columnCounter = 0; columnCounter < 4; columnCounter++) {
                    
                    if(counter == 16){
                        
                        break rowLoop;                                              // Jump out of the loop
                    } else{
                        
                        table.setValueAt(out.get(counter), indice[rowCounter], indice[columnCounter]);   // set value at the specific position
                        counter++;                                                  // increment counter
                    }// end if
                }// end for columnLoop:
            }// end for rowLoop
        }// end if 
        
        else if(kMapInputs.cycle.equalsIgnoreCase("Up Counter") || 
                kMapInputs.cycle.equalsIgnoreCase("Down Counter")){ //else if down or up counter
            rowLoop:
            for (int rowCounter = 0; rowCounter < 4; rowCounter++) {

                columnLoop:
                for (int columnCounter = 0; columnCounter < 2; columnCounter++) {
                    if(counter == 8){
                        
                       break rowLoop;                                              // Jump out of the loop
                    } else{
                        
                        table.setValueAt(out.get(counter), indice[rowCounter], indice[columnCounter]);   // set value at the specific position
                        counter++;                                                  // increment counter
                    }// end if
                }// end for columnLoop
            }// end for rowLoop
        }// end if
    }// end method fillKmap

    private static JList buildRowHeader(final JTable table) {
        final Vector<String> headers = new Vector<>();
        
        // Rowheaders
        headers.add("00");
        headers.add("01");
        headers.add("11");
        headers.add("10");

        ListModel lModel = new AbstractListModel() {

            @Override
            public int getSize() {
                return headers.size();
            }// end method getSize

            @Override
            public Object getElementAt(int index) {
                return headers.get(index);
            }// end method getElementAt
        };

        final JList rowHeader = new JList(lModel);                  // add model of headers on the JList
        rowHeader.setOpaque(true);                                  // set rowHeader List to visible
        rowHeader.setFixedCellWidth(40);                            // set the width of the header cell
        rowHeader.setCellRenderer(new RowHeaderRenderer(table));    
        rowHeader.setBackground(null);
        rowHeader.setForeground(table.getForeground());
        
        return rowHeader;
    }// end method buildRowHeader

    private static class RowHeaderRenderer extends JLabel implements ListCellRenderer {

        private final JTable outputTable;
        
        // constructor
        RowHeaderRenderer(JTable table) {
            outputTable = table;
            JTableHeader header = outputTable.getTableHeader();
            setOpaque(false);
            
            setHorizontalAlignment(CENTER);
            setForeground(header.getForeground());
            setBackground(Color.RED);
            setFont(header.getFont());
            setDoubleBuffered(true);
        }// end constructor

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            setText((value == null) ? "" : value.toString());
            setPreferredSize(null);
            setPreferredSize(new Dimension((int) getPreferredSize().getWidth(), outputTable.getRowHeight(index)));
            list.firePropertyChange("cellRenderer", 0, 1);
            return this;
        }// end method getListCellRendererComponent
    }// end class RowHeaderRenderer
}// end class KarnaughMaps
