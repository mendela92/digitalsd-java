package digitalsd;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * source of the code was from 
 * http://stackoverflow.com/questions/24555289/changing-cell-color-without-changing-color-in-other-cells-jtable
 * posted by Marco13
 * 
 * @Edited by Nelson
 * @ 17/01/2016
 */
 public class ColoringCell extends DefaultTableCellRenderer{
     
    // Varibles
    private final Map<Point, Color> cellColors = new HashMap<>();

    protected void setCelColor(ArrayList <Integer> all, Color color) {
        if (color == null){ // no color
            cellColors.remove(new Point(all.get(0), all.get(1))); 
            
        } else { // else set colour
            cellColors.put(new Point(all.get(0), all.get(1)), color);
        }// end if
    }// end method setCelColor
     
    private Color getCellColor(int r, int c){
        Color color = cellColors.get(new Point(r,c));
        if (color == null){
            return null;
        }// end if
        return color;
    }// end method getCellColor
     
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
        boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(
            table, value, isSelected, hasFocus, row, column);
        Color color = getCellColor(row, column);
        setBackground(color);
        return this;
    }// end method getTableCellRendererComponent
}// end class CellColoringCell
