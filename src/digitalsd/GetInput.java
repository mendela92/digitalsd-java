package digitalsd;

import java.io.File;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */

public class GetInput {

   // Variables Instances
   private static final GetInput getInput = new GetInput();

   protected ArrayList <String> decimalSequence = new ArrayList <>();
   protected ArrayList <String> binarySequence = new ArrayList <>();
   
   protected String cycle = "";
   protected String sequenceOptionField = "";
   
   protected int numberOfBits = 0;
   protected int optionChoosen = 0;
   protected int numberOfInputs = 0;
   protected String portUsed = "";
   
   protected File dsdDir;
   
   
   /* 
    * A private Constructor prevents any other class from instantiating.
    */
    private GetInput(){}
   
    /* Static 'instance' method */
    protected static GetInput getInstance( ) {
      return getInput;
   }// end method GetInput
 
    // Generating inputs subHeaders for next state table
    protected String [] subHeaderInputsNames(){
        // Local variable
        String [] variableName;
        
        if(cycle.equals("Up/Down Counter")){
            variableName = new String [numberOfBits+1];
            
            for (int variableCounter = 0; variableCounter < numberOfBits; variableCounter++) {
                variableName[variableCounter] = "Q"+variableCounter;
            } //end for
            variableName[numberOfBits] = "Y";
            
        } else{
            variableName = new String [numberOfBits];
            for (int variableCounter = 0; variableCounter < numberOfBits; variableCounter++) {
                variableName[variableCounter] = "Q"+variableCounter;
            }// end for
        }// end if
         
        return variableName;
    }// end method subHeaderInputsNames
    
    // Generating outputs subHeaders for next state table
    protected String [] subHeaderOuputNames(){
        // Local Variables
        String [] variableName;
        int counter = 0;
        
        //Generating number of bits for UP and Down cycle
        if(cycle.equals("Up/Down Counter")){
            variableName = new String [4*numberOfBits];
            
            for (int variableCounter = 0; variableCounter < variableName.length; variableCounter+=4) {
                variableName[variableCounter] = "J"+String.valueOf(counter)+"d";
                variableName[variableCounter+1] = "J"+String.valueOf(counter)+"u";
                variableName[variableCounter+2] = "K"+String.valueOf(counter)+"d";
                variableName[variableCounter+3] = "K"+String.valueOf(counter)+"u";
                counter++;
            }// end for
            return variableName;
            
        } else{ //Generating number of bits for UP or Down cycle
            variableName = new String [2*numberOfBits];
            
            for (int variableCounter = 0; variableCounter < variableName.length; variableCounter+=2) {
                
                variableName[variableCounter] = "J"+String.valueOf(counter);
                variableName[variableCounter+1] = "K"+String.valueOf(counter);
                counter++;
            }// end for
            return variableName;
        }// end if
    }// end method subHeaderOuputNames
    
    // Generating empty rows for Next state table
    protected Object [] generateRows(){
        // Local Variable
        Object [] rowModel;
        
        // Generating rows for Up and Down counter cycle
        if(cycle.equals("Up/Down Counter")){
            rowModel = new Object[7*numberOfBits];
            for (int rowModelCount = 0; rowModelCount < rowModel.length; rowModelCount++) {
                rowModel[rowModelCount] = new Object();
                rowModel[rowModelCount] = "";
            }// end for   
            
        } else{ // Generating rows for Up and Down counter cycle
            rowModel = new Object[4*numberOfBits];
            for (int rowModelCount = 0; rowModelCount < rowModel.length; rowModelCount++) {
                rowModel[rowModelCount] = new Object();
                rowModel[rowModelCount] = "";
            }// end for
        }// end if
        
        return rowModel;
    }// end method generateRows
    
    // Convert number in binary of 3 bits
    protected String convertDecToBin(int dec){
        // Local variable
        String bin = "";
        
        try {
            // convert to binary
            if(numberOfBits == 3){
                bin = String.format("%03d", Integer.parseInt(Integer.toBinaryString(dec))); 
                
            } else if(numberOfBits == 4){
                bin = String.format("%04d", Integer.parseInt(Integer.toBinaryString(dec))); 
            }// end if
           
        }// end try 
        catch (NumberFormatException e) {
            displayMessage("Could not convert the value in binary", "INPUT NUMBER ERROR!!!");
        }// end catch
        
        return bin;
    }// end method convertDecToBin
    
    // Exit confirmation
    protected static void closingWindow(JFrame frame){
     
        int confirm = JOptionPane.showConfirmDialog(frame, "Are you sure you want to exit?", "Exit Confirmation", JOptionPane.YES_NO_OPTION);
        
        if (confirm == JOptionPane.YES_OPTION){
            
            frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        }  else{
            
            frame.setDefaultCloseOperation(javax.swing.JFrame.DO_NOTHING_ON_CLOSE);
        }// end if
    }// end method closingWindow
    
    // Feedback message
    protected void displayMessage(String message, String title){
        
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE);
        
    }// end method displayMessage 
    
    // Verify existence of digitalSD directory
    protected boolean isDirectory(){
        
        // Local variables
        String dirPath;
        
        // Definign default saving folder
        dirPath = System.getProperty("user.home") + File.separator + "Documents"; 
        dirPath += File.separator + "DigitalSD";
        
        dsdDir = new File(dirPath);
        // if the folder had not been created then create new folder
        if (!dsdDir.exists()){
            dsdDir.mkdirs();
            return true;
        }// end if
        else if (dsdDir.exists()){ // if folder folder exists already return true
            
            return true;
        } // end if        
        return false;
    } // end method isDirectory
}// end class GetInput

