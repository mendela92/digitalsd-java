package digitalsd;

import de.javasoft.plaf.synthetica.SyntheticaBlackEyeLookAndFeel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */
public class DigitalSDCput {

    // Instance Varialbe
    public static InputForm launchApp;
    
    /**
     * @param args the command line arguments
     * @throws java.text.ParseException
     * @throws javax.swing.UnsupportedLookAndFeelException
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     */
    
    
    public static void main(String args[]) throws ParseException, UnsupportedLookAndFeelException, 
            ClassNotFoundException, InstantiationException, IllegalAccessException, Exception {
        
        // Set default Look and feel 
              UIManager.setLookAndFeel(new SyntheticaBlackEyeLookAndFeel());

        
        Thread launchThread = new Thread(){
            @Override public void run(){
                /* Create and display the form */
                launchApp = new InputForm();                    // instantiate frame 
                launchApp.setVisible(true);                     // set visible of the frame
                launchApp.setLocationRelativeTo(null);          // display frame in the middle of the screen

                launchApp.addWindowListener(new WindowAdapter() {

                    @Override public void windowClosing(WindowEvent e){

                        GetInput.closingWindow(launchApp);
                    }// end method windowClosing
                });
            }// end method run
        };launchThread.start();        
    }// end method main
}// end class DigitalSDCput
