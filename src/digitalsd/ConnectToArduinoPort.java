
package digitalsd;

import com.fazecast.jSerialComm.SerialPort;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.PrintWriter;

/**
 * Code found in a tutorial on youtube (https://www.youtube.com/watch?v=5N30jHMhw9c)
 * Published on 27 Jun 2015 by upgrdman
 * 
 * 
 *           DigitalSD
 * @Modified by Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */

public class ConnectToArduinoPort extends javax.swing.JFrame {
    
    // Instances variables
    private static SerialPort choosenPort;
    private SerialPort [] portName = null;
    private GetInput arduinoSequence;
    private boolean sendingData = true;
    private boolean detectedPorts = false;
    
    // Constructor if not connected
    public ConnectToArduinoPort() {
        
        // Set Icon
        setIcon();
        
        // auto components generated
        initComponents();
        
        // Getting available ports
        gettingPorts();
    }// end constructor

    private void gettingPorts(){
        Thread getPortThread = new Thread(){
            @Override
            public void run(){
 
                portNameComboBox.setEnabled(false); // Disable portComboBox
                statusConnectionLabel.setForeground(Color.RED); // set color to red
                statusConnectionLabel.setText("Please Wait !!!"); // SetText to Please Wait !!!
                connectButton.setEnabled(false); // Disable connect button

                portName = SerialPort.getCommPorts();   // Get Available ports

                // Populate ComboBox of available ports
                for (SerialPort portName1 : portName) {
                    portNameComboBox.addItem(portName1.getSystemPortName()); // populate with 
                } // end for
                detectedPorts = true; 
                
                if(detectedPorts == true){
                    portNameComboBox.setEnabled(true); // Enable portComboBox
                    statusConnectionLabel.setForeground(null); // set font colour to default
                    statusConnectionLabel.setText("Not Connected"); // setText to Not Connected
                    connectButton.setEnabled(true); // Enable connect button
                    portNameComboBox.requestFocus();
                }// end if
            }// end method run
        }; getPortThread.start(); // start thread
        
        
    }// end method gettingPorts
    
    // Constructor if connected
    public ConnectToArduinoPort(String prtUsed) {
        
        // Set Icon
        setIcon();
        
        // auto components generated
        initComponents();
        
        portNameComboBox.addItem(prtUsed);          // set current port used
        portNameComboBox.setEnabled(false);         // disable combobox
        connectButton.setText("Disconnect");        // Set Connect Button text
        statusConnectionLabel.setText("Connected"); // Set Status
        
    }// end Constructor if connected
    
    private void connectToHardware(){
        // Local varialble
        String sequenceTosend = "";
             
        // attempt to connect to the serial port
        if(connectButton.getText().equals("Connect")){

            // Connectiong to the selected ports;
            choosenPort = SerialPort.getCommPort(portNameComboBox.getSelectedItem().toString());

            // Waiting for inputs
            choosenPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0);

            // Check if Port cnnected
            if(choosenPort.openPort()){

                connectButton.setText("Disconnect");                    // set button text to Disconect
                portNameComboBox.setEnabled(false);                     // Disable comboBox SO that user can change ports
                statusConnectionLabel.setText("Connected");             // status of connexion
                arduinoSequence.portUsed = portNameComboBox.getSelectedItem().toString();   // store used ports
                
                

                // Wait for 2 seconds and hide frame
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {}// end catch
                dispose();
                
                PrintWriter out = null;
                try {
                    // Preparing to write sequence via the serial port
                    out = new PrintWriter(choosenPort.getOutputStream());
                } catch (NullPointerException e ) {
                    arduinoSequence.displayMessage("Sequence was not sent via the serial port", "Sequence not sent !!!");
                }// end catch
                

                // Sequence to Send to arduino
                for (String decimalSequence : arduinoSequence.decimalSequence) {
                    sequenceTosend = sequenceTosend.concat(decimalSequence);
                }// end for

                // enter infinte loop that send sequence to arduino over and over
                while (sendingData) {
                    out.print(sequenceTosend);
                    out.flush();
                    try {Thread.sleep(2500);} catch(Exception e){}

                }// end while

            } else { // if you could not connect

                Toolkit.getDefaultToolkit().beep();
                statusConnectionLabel.setText("Could not connect");
            }// end if
        } else{
            // disconnect from the serial port
            sendingData = false;                                // stop sending data
            choosenPort.closePort();                            // Close port
            connectButton.setText("Connect");                   // set connect button text
            statusConnectionLabel.setText("Not Connected");     // set status of connection
            arduinoSequence.portUsed = "";                      // initialize port selected

            // Wait for 2 seconds and hide frame
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {}    // end catch
            dispose();                              // close current window
        }// end if
    }// end method connect
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        portNameComboBox = new javax.swing.JComboBox();
        connectButton = new javax.swing.JButton();
        statusLabel = new javax.swing.JLabel();
        connectionPortLabel = new javax.swing.JLabel();
        statusConnectionLabel = new javax.swing.JLabel();
        instructionLabel = new javax.swing.JLabel();
        instructionLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Connecting DSDCircuit");
        setResizable(false);
        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
            }
            public void windowLostFocus(java.awt.event.WindowEvent evt) {
                formWindowLostFocus(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        portNameComboBox.setBackground(new java.awt.Color(255, 51, 51));
        portNameComboBox.setFont(new java.awt.Font("Cambria", 1, 12)); // NOI18N
        portNameComboBox.setMaximumRowCount(3);
        portNameComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                portNameComboBoxKeyPressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 25, 2, 2);
        getContentPane().add(portNameComboBox, gridBagConstraints);

        connectButton.setFont(new java.awt.Font("Cambria", 0, 13)); // NOI18N
        connectButton.setText("Connect");
        connectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 3, 5);
        getContentPane().add(connectButton, gridBagConstraints);

        statusLabel.setFont(new java.awt.Font("Cambria", 0, 12)); // NOI18N
        statusLabel.setText("Status:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        getContentPane().add(statusLabel, gridBagConstraints);

        connectionPortLabel.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        connectionPortLabel.setText("Connect to a Port:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(5, 25, 5, 5);
        getContentPane().add(connectionPortLabel, gridBagConstraints);

        statusConnectionLabel.setFont(new java.awt.Font("Cambria", 1, 13)); // NOI18N
        statusConnectionLabel.setText("Not Connected");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        getContentPane().add(statusConnectionLabel, gridBagConstraints);

        instructionLabel.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        instructionLabel.setText("Choose the correct arduino COM port");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(15, 10, 5, 10);
        getContentPane().add(instructionLabel, gridBagConstraints);

        instructionLabel1.setFont(new java.awt.Font("Cambria", 0, 12)); // NOI18N
        instructionLabel1.setForeground(new java.awt.Color(255, 51, 51));
        instructionLabel1.setText("See User Manual for more details");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 5, 10);
        getContentPane().add(instructionLabel1, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void connectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connectButtonActionPerformed
        
        arduinoSequence = GetInput.getInstance();
        
         Thread connectionThread; connectionThread = new Thread(){
             @Override public void run(){
                 
                 connectToHardware();
             }// end method run
             
         }; connectionThread.start();               // Start thread
    }//GEN-LAST:event_connectButtonActionPerformed

    private void formWindowLostFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowLostFocus
        dispose(); // close current window
    }//GEN-LAST:event_formWindowLostFocus

    private void portNameComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_portNameComboBoxKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            connectToHardware(); // connect to hardware
        }// end if
    }//GEN-LAST:event_portNameComboBoxKeyPressed

    // Set Icon
    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/digitalsequencedesigner/ImagesDSD/Logo Icon.png")));
    }// end method setIcon
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton connectButton;
    private javax.swing.JLabel connectionPortLabel;
    private javax.swing.JLabel instructionLabel;
    private javax.swing.JLabel instructionLabel1;
    private javax.swing.JComboBox portNameComboBox;
    private javax.swing.JLabel statusConnectionLabel;
    private javax.swing.JLabel statusLabel;
    // End of variables declaration//GEN-END:variables
}// end class ConnectToArduinoPort
