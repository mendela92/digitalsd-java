
package digitalsd;

/**
 * NextStateGroupableTableHeader
 * 
 * From http://www.crionics/com/products/opensource/faq/swing_ex/ swingExamples.html
 * found on www.java2s.com/Code/java/Swing-Component/ GroupHeaderExample.htm
 * 
 * 
 * Modified by: Nobuo Tamemasa
 * @version 1.0 20.10.1998
 * @author Nobuo Tamemasa
 * 
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

@SuppressWarnings("serial")
public class NextStateGroupableTableHeader extends JTableHeader {
 

    protected List<NextStateColumnGroup> columnGroups = new ArrayList<>();

    // Constructor
    protected NextStateGroupableTableHeader(TableColumnModel model) {
        super(model);
        setUI(new NextStateGroupableTableHeaderUI());
        setReorderingAllowed(false);
        setResizingAllowed(false);
    }// end constructor

    @Override
    public void updateUI() {
        setUI(new NextStateGroupableTableHeaderUI());
    }// end method updateUI

    @Override
    public final void setReorderingAllowed(boolean b) {
        super.setReorderingAllowed(false);
    }// end method setReorderingAllowed
    
    @Override
    public final void setResizingAllowed(boolean b){
	super.setResizingAllowed(false);
    }// end method setResizingAllowed

    protected void addColumnGroup(NextStateColumnGroup g) {
        columnGroups.add(g);
    }// end method addColumnGroup

    protected List<NextStateColumnGroup> getColumnGroups(TableColumn col) {
        for (NextStateColumnGroup group : columnGroups) {
            List<NextStateColumnGroup> groups = group.getColumnGroups(col);
            if (!groups.isEmpty()) {
                return groups;
            }// end if
        }// end for
        
        return Collections.emptyList();
    }// end method getColumnGroups

    protected void setColumnMargin() {
        int columnMargin = getColumnModel().getColumnMargin();
        for (NextStateColumnGroup group : columnGroups) {
            group.setColumnMargin(columnMargin);
        }// end for 
    }// end method setColumnMargin

}// end class NextStateGroupableTableHeader
