package digitalsd;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;


/**
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */ 

public class KMapAndExpressionForm extends javax.swing.JPanel {

    protected KarnaughMaps KMaps;
    /**
     * Creates new form kMapWithExpression
     * @param outputArray
     * @param orValue
     * @param andValue
     * @param out
     * @param expression
     */
    public KMapAndExpressionForm(ArrayList<String> outputArray, String expression, int orValue, int andValue, String out) {
        
        // Initialize components
        initComponents();
        
        KMaps = new KarnaughMaps(outputArray);
        KMaps.table.setToolTipText(out+ " K-Maps table"); // set tool tip
        kMapPanel.add(KMaps); // add panel
        outputScrollPane.setBorder(BorderFactory.createTitledBorder(new SoftBevelBorder(BevelBorder.RAISED), out+ " Expression", 
                TitledBorder.ABOVE_TOP, TitledBorder.DEFAULT_POSITION, new Font("Cambria", 1, 15), Color.gray)); // Set title border

        outputField.setText(expression);
        
        String [] splitLogicExpression;
        
        if(orValue == 0){
            
            // Expression to countthe number of Gate
            splitLogicExpression = expression.split("(?!^)");
            
            if (splitLogicExpression.length >= 4 ){
                outputField.setToolTipText(out + " need only " + andValue + " AND gates");
                
            } else{
                switch (expression) {
                    case "1":
                        outputField.setToolTipText(out + " is directly connected to Vcc");
                        break;
                    case "0":
                        outputField.setToolTipText(out + " is directly connected to Ground");
                        break;
                    default:
                        outputField.setToolTipText(out + " is directly connected to " + expression);
                        break;
                }// end switch
            }// end inner if
            
        } else if(andValue == 0){
            outputField.setToolTipText(out + " need only " + orValue + " OR gates");
            
        } else if (orValue != 0 && andValue != 0){
            outputField.setToolTipText(out + " need " + orValue + " OR gates and " + andValue + " AND Gates");
        }// end if
        
    }// end constructor

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        kMapPanel = new javax.swing.JPanel();
        outputScrollPane = new javax.swing.JScrollPane();
        outputField = new javax.swing.JTextField();

        setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        add(kMapPanel, gridBagConstraints);

        outputScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        outputField.setEditable(false);
        outputField.setBackground(new java.awt.Color(204, 204, 204));
        outputField.setFont(new java.awt.Font("Cambria", 3, 15)); // NOI18N
        outputField.setForeground(new java.awt.Color(255, 0, 0));
        outputField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        outputField.setToolTipText("");
        outputField.setBorder(null);
        outputField.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        outputField.setFocusable(false);
        outputField.setPreferredSize(new java.awt.Dimension(300, 30));
        outputScrollPane.setViewportView(outputField);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(outputScrollPane, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel kMapPanel;
    private javax.swing.JTextField outputField;
    private javax.swing.JScrollPane outputScrollPane;
    // End of variables declaration//GEN-END:variables
}// end class KMapAndExpressionForm
