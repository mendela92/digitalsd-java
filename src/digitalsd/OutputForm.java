package digitalsd;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */

public class OutputForm extends javax.swing.JFrame {

    // Instances Variables
    private final GetInput output;
    private final GridBagConstraints fieldConstraints;

    private JTextField starterField;
    private JTextField [] sQFields;

    private JLabel[] stateLabel;
    private JLabel starterLabel;

    private CountingUp sequenceCountingUp;
    private NextStateTable nextStateTable;
   
    private int counterSequence;
    private int ipax;
    
    public OutputForm() {
           
        // set Icon to the App
        setIcon();
        initComponents();
        
        this.fieldConstraints = new GridBagConstraints();
        fieldConstraints.anchor = GridBagConstraints.CENTER;
        this.output = GetInput.getInstance();
        
        // Determine and display output of the design
        outputDesignedCounter();

        sequenceCountingThread();
    }// end constructor

    private void cellColoring(QuineMcCluskeyMethod outputExpression, KMapAndExpressionForm outputkMaps, Color color){
        
        // Local Variables
        final ColoringCell kMapCellRenderer = new ColoringCell();
        TableColumnModel kMapColumnModel = outputkMaps.KMaps.table.getColumnModel();

        for (int columnModelCount =0; columnModelCount < kMapColumnModel.getColumnCount(); columnModelCount++){
            TableColumn column = kMapColumnModel.getColumn(columnModelCount);
            column.setCellRenderer(kMapCellRenderer);
        }// end for

        for (int coloringCounter = 0; coloringCounter < outputExpression.getSolutionidx().size(); coloringCounter++) {
            kMapCellRenderer.setCelColor(getEquivalentColAndRow(outputExpression.getSolutionidx().get(coloringCounter)), color);
        }// end for
        
        outputkMaps.KMaps.table.repaint();
    }// end method cellCoring
    
    // get the corresponding row and column of a minterms
    private ArrayList <Integer> getEquivalentColAndRow(int i){
        // Local variables
        ArrayList <Integer> colAndRow = new ArrayList <>();
        
        // Translate minterms into coordinate of the k-maps table
        if(output.numberOfBits == 3 && output.cycle.equals("Up Counter") || output.cycle.equals("Down Counter")){
            if(i == 0){
                colAndRow.add(0);
                colAndRow.add(0);
            } else if(i == 1){
                colAndRow.add(0);
                colAndRow.add(1);
            } else if(i == 2){
                colAndRow.add(1);
                colAndRow.add(0);
            } else if(i == 3){
                colAndRow.add(1);
                colAndRow.add(1);
            } else if(i == 4){
                colAndRow.add(3);
                colAndRow.add(0);
            } else if(i == 5){
                colAndRow.add(3);
                colAndRow.add(1);
            } else if(i == 6){
                colAndRow.add(2);
                colAndRow.add(0);
            } else if(i == 7){
                colAndRow.add(2);
                colAndRow.add(1);
            } // end if
        }  else {
            if(i == 0){
                colAndRow.add(0);
                colAndRow.add(0);
            } else if(i == 1){
                colAndRow.add(0);
                colAndRow.add(1);
            } else if(i == 2){
                colAndRow.add(0);
                colAndRow.add(3);
            } else if(i == 3){
                colAndRow.add(0);
                colAndRow.add(2);
            } else if(i == 4){
                colAndRow.add(1);
                colAndRow.add(0);
            } else if(i == 5){
                colAndRow.add(1);
                colAndRow.add(1);
            } else if(i == 6){
                colAndRow.add(1);
                colAndRow.add(3);
            } else if(i == 7){
                colAndRow.add(1);
                colAndRow.add(2);
            } else if(i == 8){
                colAndRow.add(3);
                colAndRow.add(0);
            } else if(i == 9){
                colAndRow.add(3);
                colAndRow.add(1);
            } else if(i == 10){
                colAndRow.add(3);
                colAndRow.add(3);
            } else if(i == 11){
                colAndRow.add(3);
                colAndRow.add(2);
            } else if(i == 12){
                colAndRow.add(2);
                colAndRow.add(0);
            } else if(i == 13){
                colAndRow.add(2);
                colAndRow.add(1);
            } else if(i == 14){
                colAndRow.add(2);
                colAndRow.add(3);
            } else if(i == 15){
                colAndRow.add(2);
                colAndRow.add(2);
            }// end inner
        }//end outer if
        return colAndRow;   
    }// end method getColAndRow
    
    private void outputDesignedCounter(){
        Thread nextState_KmapThread = new Thread(){
            @Override
            public void run(){
                
                // State Diagram display
                stateDiagram(output.numberOfInputs);
        
                // Next State Table
                nextStateTable = new NextStateTable();
                nextStatePanel.add(nextStateTable);

                // karnaugh Maps sequence
                karnaughMaps(nextStateTable, null);

                showGroupingMenuItem.setSelected(false); // deactivate grouping
            }
        }; nextState_KmapThread.start();
    }// end method setNextState_kmap
    
    // displaying sequence entered
    private void sequenceCountingThread(){
        
        sequenceCountingUp = new CountingUp();
        sequenceCountingUp.start();
     
    }// end method sequenceCountingThread
    
    // State Diagram ----------------------------------------------------------------------------------------------------------//

    private void displayBinaryInput (){
       
        //Set binary values on the textField for State diagram
        for(int i = 0; i < output.numberOfInputs; i++){
            sQFields[i].setText((String)output.binarySequence.get(i));
        }// end for
        
        // only there is a starter option display the binary equivalent
        if(output.optionChoosen == 1){
             starterField.setText((String)output.binarySequence.get(output.binarySequence.size()-1));
        }// end if
 
    }// end method getInput// end method setStateDiagram

    private void displayDecimalInput (){
       
        //Set Decimal values on the textField for State diagram
        for(int i = 0; i < output.numberOfInputs; i++){
            stateLabel[i].setText((String)output.decimalSequence.get(i));
        }// end for
        
        // convert decimal inputs to binary
        for (String decimalSequence : output.decimalSequence) {
            output.binarySequence.add(output.convertDecToBin(Integer.parseInt((String) decimalSequence)));
        }// end for
        
        if(output.optionChoosen == 1){// if starter option is selected

            // set label starter
            starterLabel.setText(output.sequenceOptionField);
            output.binarySequence.add(output.convertDecToBin(Integer.parseInt(output.sequenceOptionField))); 
        }// end if
    }// end method setStateDiagram
    
    private void illegalStates(){
        
        // Local Variables
        ArrayList <String> fullSequence; 
        ArrayList <String> illegalStateNumbers;
        
        fullSequence = new ArrayList <>();
        illegalStateNumbers = new ArrayList <>();
        
        // Generate a 3 Bits sequence number (0-7)
        if(output.numberOfBits == 3){
            generate3BitsLoop:
            for(int i = 0; i < 8; i++){
                fullSequence.add(String.valueOf(i));
            }// end for generate3BitsLoop
        }else{
            generate3BitsLoop:
            for (int i = 0; i < 16; i++)
            {
                fullSequence.add(String.valueOf(i));
            }// end for generate3BitsLoop
        }// end for
        

        // Determineinf illegal states numbers
        String illegal ="";

        completeSeqenceLoop:
        for (String sequence : fullSequence) {
            if (!output.decimalSequence.contains(sequence)) {
                illegalStateNumbers.add(sequence);
                illegal += sequence.concat(", ");
            } // end if
        } // end for completeSeqenceLoop

        stateMessageLabel.setText( illegal + " Attached to " + output.sequenceOptionField);

    }// end method setIllegalStates  
    
    private void starterNumber(){
        
        // add State Label
        fieldConstraints.gridx = 0;
        fieldConstraints.gridy = 0;
        stateDiagramPanel.add(starterLabel, fieldConstraints);   

        // add starter field
        fieldConstraints.gridx = 0;
        fieldConstraints.gridy = 1;
        fieldConstraints.ipadx = ipax;
        fieldConstraints.ipady = 1;
        stateDiagramPanel.add(starterField, fieldConstraints);

        stateMessageLabel.setText("Sequence started at " + output.sequenceOptionField);
    }// end method starterNumber

    private void stateDiagram(final int sequenceLength){
        
        //Instantiate variables
        sQFields = new JTextField[sequenceLength];
        stateLabel = new JLabel [sequenceLength];
       
        // Initialize Starter number field
        starterField = new JTextField(1);
        starterField.setEditable(false);
        starterField.setFocusable(false);   
        starterField.setHorizontalAlignment(JTextField.CENTER); 
        starterField.setFont(new Font("Cambria", Font.PLAIN, 15));
        
        // Initialize Starter number label
        starterLabel = new JLabel();
        starterLabel.setFont(new Font("Cambria", Font.BOLD, 17));     
        starterLabel.setForeground(Color.RED);

        Thread generateStateDiagram = new Thread(){
       
            @Override
            public void run() {
                
                // Generate Field according to the numbers inputs
                for(int i = 0; i < sequenceLength; i++){
                    sQFields [i] = new JTextField(1);
                    sQFields [i].setPreferredSize(new Dimension(50, 30));
                    sQFields [i].setEditable(false);
                    sQFields [i].setFocusable(false);
                    sQFields [i].setHorizontalAlignment(JTextField.CENTER); 
                    sQFields [i].setFont(new Font("Cambria", Font.PLAIN, 15));
                    
                    // Decimal value of the counter
                    stateLabel [i] = new JLabel(String.valueOf(i));
                    stateLabel [i].setForeground(Color.RED);
                    stateLabel [i].setHorizontalAlignment(JTextField.CENTER);
                    stateLabel [i].setFont(new Font("Cambria", Font.BOLD, 17));
                }// end if
                
                if(output.numberOfBits == 3){
                    ipax = 25;
                }else{
                    ipax = 35;
                }// end if
                // adding TextField according to the number of inputs
                switch (sequenceLength-3){
                    
                    case 1:// 4 inputs
                        
                        // add State Label
                        fieldConstraints.gridx = 3;
                        fieldConstraints.gridy = 1;
                        stateDiagramPanel.add(stateLabel[0], fieldConstraints);
                        
                        // Add Sequence Field
                        fieldConstraints.gridx = 3;
                        fieldConstraints.gridy = 2;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[0], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 6;
                        fieldConstraints.gridy = 4;
                        stateDiagramPanel.add(stateLabel[1], fieldConstraints);
                        
                        // Add Sequence Field
                        fieldConstraints.gridx = 5;
                        fieldConstraints.gridy = 4;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[1], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 3;
                        fieldConstraints.gridy = 7;
                        stateDiagramPanel.add(stateLabel[2], fieldConstraints);
                        
                        // Add Sequence Field
                        fieldConstraints.gridx = 3;
                        fieldConstraints.gridy = 6;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[2], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 0;
                        fieldConstraints.gridy = 4;
                        stateDiagramPanel.add(stateLabel[3], fieldConstraints);
                        
                        // Add Sequence Field
                        fieldConstraints.gridx = 1;
                        fieldConstraints.gridy = 4;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[3], fieldConstraints);

                    break;

                case 2:// 5 inputs
                    
                        // add State Label
                        fieldConstraints.gridx = 4;
                        fieldConstraints.gridy = 1;
                        stateDiagramPanel.add(stateLabel[0], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 4;
                        fieldConstraints.gridy = 2;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[0], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 6;
                        fieldConstraints.gridy = 2;
                        stateDiagramPanel.add(stateLabel[1], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 6;
                        fieldConstraints.gridy = 3;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[1], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 5;
                        fieldConstraints.gridy = 7;

                        stateDiagramPanel.add(stateLabel[2], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 5;
                        fieldConstraints.gridy = 6;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[2], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 3;
                        fieldConstraints.gridy = 7;
                        stateDiagramPanel.add(stateLabel[3], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 3;
                        fieldConstraints.gridy = 6;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[3], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 2;
                        fieldConstraints.gridy = 2;
                        stateDiagramPanel.add(stateLabel[4], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 2;
                        fieldConstraints.gridy = 3;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        fieldConstraints.anchor = java.awt.GridBagConstraints.CENTER;
                        stateDiagramPanel.add(sQFields[4], fieldConstraints);

                        break;
                    case 3:// 6 inputs   

                        // add State Label
                        fieldConstraints.gridx = 3;
                        fieldConstraints.gridy = 1;
                        stateDiagramPanel.add(stateLabel[0], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 3;
                        fieldConstraints.gridy = 2;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[0], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 5;
                        fieldConstraints.gridy = 1;
                        stateDiagramPanel.add(stateLabel[1], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 5;
                        fieldConstraints.gridy = 2;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[1], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 6;
                        fieldConstraints.gridy = 2;
                        stateDiagramPanel.add(stateLabel[2], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 6;
                        fieldConstraints.gridy = 3;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[2], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 5;
                        fieldConstraints.gridy = 6;
                        stateDiagramPanel.add(stateLabel[3], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 5;
                        fieldConstraints.gridy = 5;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[3], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 3;
                        fieldConstraints.gridy = 6;
                        stateDiagramPanel.add(stateLabel[4], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 3;
                        fieldConstraints.gridy = 5;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[4], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 2;
                        fieldConstraints.gridy = 2;
                        stateDiagramPanel.add(stateLabel[5], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 2;
                        fieldConstraints.gridy = 3;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[5], fieldConstraints);

                    break;

                case 4:// 7 inputs  

                        // add State Label
                        fieldConstraints.gridx = 4;
                        fieldConstraints.gridy = 0;
                        stateDiagramPanel.add(stateLabel[0], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 4;
                        fieldConstraints.gridy = 1;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[0], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 6;
                        fieldConstraints.gridy = 1;
                        stateDiagramPanel.add(stateLabel[1], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 6;
                        fieldConstraints.gridy = 2;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[1], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 7;
                        fieldConstraints.gridy = 4;
                        stateDiagramPanel.add(stateLabel[2], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 6;
                        fieldConstraints.gridy = 4;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[2], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 5;
                        fieldConstraints.gridy = 7;
                        stateDiagramPanel.add(stateLabel[3], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 5;
                        fieldConstraints.gridy = 5;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[3], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 3;
                        fieldConstraints.gridy = 7;
                        stateDiagramPanel.add(stateLabel[4], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 3;
                        fieldConstraints.gridy = 5;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[4], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 1;
                        fieldConstraints.gridy = 4;
                        stateDiagramPanel.add(stateLabel[5], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 2;
                        fieldConstraints.gridy = 4;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[5], fieldConstraints);

                        // add State Label
                        fieldConstraints.gridx = 2;
                        fieldConstraints.gridy = 1;
                        stateDiagramPanel.add(stateLabel[6], fieldConstraints);

                        // Add Sequence Field
                        fieldConstraints.gridx = 2;
                        fieldConstraints.gridy = 2;
                        fieldConstraints.ipadx = ipax;
                        fieldConstraints.ipady = 1;
                        stateDiagramPanel.add(sQFields[6], fieldConstraints);

                    break;   //
                
                }// end switch
                
                if(output.optionChoosen == 0){
                    stateMessageLabel.setText("Sequence only will be counting");
                }
                else if(output.optionChoosen == 1){ // if starter option is selected
                    starterNumber();
                }
                else if(output.optionChoosen == 2){ // Set Illegal state if selected
                    illegalStates();
                }// end if
                

                //  display Decimal value
                displayDecimalInput();

                // display Binary value
                displayBinaryInput();
                
               
                
                // display type of cycle
                switch (output.cycle) {
                    case "Up/Down Counter":
                        upCounterLogo.setIcon(new ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/Up counter Icon.png")));
                        upCounterLabel.setText("Up Counter");
                        downCounterLogo.setIcon(new ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/Down counter Icon.png")));
                        downCounterLabel.setText("Down Counter");
                        break;
                    case "Up Counter":
                        upCounterLogo.setIcon(new ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/Up counter Icon.png")));
                        upCounterLabel.setText("Up Counter");
                        break;
                    case "Down Counter":
                        downCounterLogo.setIcon(new ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/Down counter Icon.png")));
                        downCounterLabel.setText("Down Counter");
                        break;
                }// end switch
                
                // repaint components
                mainStateDiagramPanel.validate();
            }// end method run
        }; generateStateDiagram.start();
        
    }// end method stateDiagram
    
    private void karnaughMaps(NextStateTable nextStateTable, Color color){
        
        // Local variables
        QuineMcCluskeyMethod [] outputExpression ;
        KMapAndExpressionForm [] outputkMaps;
        
        kMapPanel.removeAll();  // Remove any components in the panel
        
        // Karnaugh Maps
        if(output.numberOfBits == 3 && output.cycle.equals("Up Counter") || output.cycle.equals("Down Counter")){
            kMapPanel.setLayout(new GridLayout(2, 3, 2, 2));    // set layout
            
            // initialize variables
            outputExpression = new QuineMcCluskeyMethod[6];     
            outputkMaps = new KMapAndExpressionForm[6];
            
            for (int i = 0; i < outputkMaps.length; i++) {
                outputExpression [i] = new QuineMcCluskeyMethod(nextStateTable.getValueInTable()[i], output.numberOfBits );
                outputkMaps[i] = new KMapAndExpressionForm(nextStateTable.getValueInTable()[i], outputExpression[i].extractExpression(), 
                        outputExpression [i].OrGateCount, outputExpression [i].andGateCount, output.subHeaderOuputNames()[i].substring(0, 2));

                kMapPanel.add(outputkMaps[i]);
                cellColoring(outputExpression [i], outputkMaps[i], color); // Coloring minterms cell
                
            }// end for
        }
        else if(output.numberOfBits == 3 && output.cycle.equals("Up/Down Counter") ){
            kMapPanel.setLayout(new GridLayout(2, 3, 2, 2));
            outputExpression = new QuineMcCluskeyMethod[6];
            outputkMaps = new KMapAndExpressionForm[6];
            int counter = 0;
            for (int i = 0; i < outputkMaps.length; i++) {
                outputExpression [i] = new QuineMcCluskeyMethod(nextStateTable.getValueInTable()[i], output.numberOfBits+1 );
                outputkMaps[i] = new KMapAndExpressionForm(nextStateTable.getValueInTable()[i], outputExpression[i].extractExpression(), 
                        outputExpression [i].OrGateCount, outputExpression [i].andGateCount, output.subHeaderOuputNames()[counter].substring(0, 2));
                counter += 2;
                kMapPanel.add(outputkMaps[i]);
                cellColoring(outputExpression [i], outputkMaps[i], color); // Coloring minterms cell
            }// end for
        }
        else if(output.numberOfBits == 4 && output.cycle.equals("Up Counter") || output.cycle.equals("Down Counter")){
            kMapPanel.setLayout(new GridLayout(2, 4));
                outputExpression = new QuineMcCluskeyMethod[8];
                outputkMaps = new KMapAndExpressionForm[8];
                
                for (int i = 0; i < outputExpression.length; i++) {
                    outputExpression [i] = new QuineMcCluskeyMethod(nextStateTable.getValueInTable()[i], output.numberOfBits );
                    outputkMaps[i] = new KMapAndExpressionForm(nextStateTable.getValueInTable()[i], outputExpression[i].extractExpression(), 
                            outputExpression [i].OrGateCount, outputExpression [i].andGateCount, output.subHeaderOuputNames()[i]);
                    kMapPanel.add(outputkMaps[i]);
                    cellColoring(outputExpression [i], outputkMaps[i], color); // Coloring minterms cell
                }// end for
        }
        else if(output.numberOfBits == 4 && output.cycle.equals("Up/Down Counter") ){
            kMapPanel.setLayout(new GridLayout(4, 4));
                outputExpression = new QuineMcCluskeyMethod[16];
                outputkMaps = new KMapAndExpressionForm[16];
                
                for (int i = 0; i < outputExpression.length; i++) {
                    outputExpression [i] = new QuineMcCluskeyMethod(nextStateTable.getValueInTable()[i], output.numberOfBits );
                    outputkMaps[i] = new KMapAndExpressionForm(nextStateTable.getValueInTable()[i], outputExpression[i].extractExpression(), 
                            outputExpression [i].OrGateCount, outputExpression [i].andGateCount, output.subHeaderOuputNames()[i]);
                    kMapPanel.add(outputkMaps[i]);
                    cellColoring(outputExpression [i], outputkMaps[i], color); // Coloring minterms cell
                }// end for
        }// end if
        kMapPanel.validate();
        kMapPanel.repaint();
    }// end method karnaughMaps
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        outputTabbedPane = new javax.swing.JTabbedPane();
        stnDiagramPanel = new javax.swing.JPanel();
        state_transitionPanel = new javax.swing.JPanel();
        mainStateDiagramPanel = new javax.swing.JPanel();
        stateDiagramScroll = new javax.swing.JScrollPane();
        stateDiagram = new javax.swing.JPanel();
        stateDiagramPanel = new javax.swing.JPanel();
        sequenceInfoPanel = new javax.swing.JPanel();
        cycleStatusPanel = new javax.swing.JPanel();
        upCounterLogo = new javax.swing.JLabel();
        upCounterLabel = new javax.swing.JLabel();
        downCounterLogo = new javax.swing.JLabel();
        downCounterLabel = new javax.swing.JLabel();
        stateMessageLabel = new javax.swing.JLabel();
        TransitionTablePanel = new javax.swing.JPanel();
        transitionTableScrollPane = new javax.swing.JScrollPane();
        transitionTable = new javax.swing.JTable();
        sequenceCountingPanel = new javax.swing.JPanel();
        sequenceLabel = new javax.swing.JLabel();
        nextStatePanel = new javax.swing.JPanel();
        kMapScroll = new javax.swing.JScrollPane();
        kMapPanel = new javax.swing.JPanel();
        DSDMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        newMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        saveAs_showGrouping_Separator = new javax.swing.JPopupMenu.Separator();
        showGroupingMenuItem = new javax.swing.JCheckBoxMenuItem();
        showGrouping_connectToHardware_Separator = new javax.swing.JPopupMenu.Separator();
        connectTohardwareMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        userManualMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("DigitalSD");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        outputTabbedPane.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        outputTabbedPane.setFont(new java.awt.Font("Cambria", 1, 16)); // NOI18N

        stnDiagramPanel.setLayout(new java.awt.BorderLayout());

        state_transitionPanel.setPreferredSize(new java.awt.Dimension(900, 260));
        state_transitionPanel.setLayout(new java.awt.GridLayout(1, 3));

        mainStateDiagramPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "State Diagram", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Cambria", 1, 22), java.awt.Color.gray)); // NOI18N
        mainStateDiagramPanel.setToolTipText("State Diagram");
        mainStateDiagramPanel.setLayout(new java.awt.GridLayout(1, 0));

        stateDiagram.setLayout(new java.awt.GridBagLayout());

        stateDiagramPanel.setToolTipText("State Diagram");
        stateDiagramPanel.setPreferredSize(new java.awt.Dimension(400, 150));
        stateDiagramPanel.setLayout(new java.awt.GridBagLayout());
        stateDiagram.add(stateDiagramPanel, new java.awt.GridBagConstraints());

        sequenceInfoPanel.setLayout(new java.awt.BorderLayout(6, 0));

        cycleStatusPanel.setToolTipText("Cycle  Status");
        cycleStatusPanel.setLayout(new java.awt.GridBagLayout());

        upCounterLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        cycleStatusPanel.add(upCounterLogo, gridBagConstraints);

        upCounterLabel.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        upCounterLabel.setForeground(new java.awt.Color(102, 255, 51));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        cycleStatusPanel.add(upCounterLabel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        cycleStatusPanel.add(downCounterLogo, gridBagConstraints);

        downCounterLabel.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        downCounterLabel.setForeground(new java.awt.Color(255, 0, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        cycleStatusPanel.add(downCounterLabel, gridBagConstraints);

        sequenceInfoPanel.add(cycleStatusPanel, java.awt.BorderLayout.WEST);

        stateMessageLabel.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        stateMessageLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        stateMessageLabel.setToolTipText("Your Sequence option");
        sequenceInfoPanel.add(stateMessageLabel, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        stateDiagram.add(sequenceInfoPanel, gridBagConstraints);

        stateDiagramScroll.setViewportView(stateDiagram);

        mainStateDiagramPanel.add(stateDiagramScroll);

        state_transitionPanel.add(mainStateDiagramPanel);

        TransitionTablePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "TransitionTable", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Cambria", 1, 22), java.awt.Color.gray)); // NOI18N
        TransitionTablePanel.setToolTipText("Transition Table");
        TransitionTablePanel.setLayout(new java.awt.GridBagLayout());

        transitionTableScrollPane.setPreferredSize(new java.awt.Dimension(270, 179));

        transitionTable.setBorder(javax.swing.BorderFactory.createMatteBorder(4, 4, 4, 4, new java.awt.Color(153, 153, 153)));
        transitionTable.setFont(new java.awt.Font("Cambria", 1, 17)); // NOI18N
        transitionTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"0", "0", "0", "X"},
                {"0", "1", "1", "X"},
                {"1", "0", "X", "1"},
                {"1", "1", "X", "1"}
            },
            new String [] {
                "Qn", "Qn+1", "J", "K"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        transitionTable.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        transitionTable.setEnabled(false);
        transitionTable.setFocusable(false);
        transitionTable.setRowHeight(36);
        transitionTable.getTableHeader().setReorderingAllowed(false);
        transitionTableScrollPane.setViewportView(transitionTable);
        if (transitionTable.getColumnModel().getColumnCount() > 0) {
            transitionTable.getColumnModel().getColumn(0).setResizable(false);
            transitionTable.getColumnModel().getColumn(1).setResizable(false);
            transitionTable.getColumnModel().getColumn(2).setResizable(false);
            transitionTable.getColumnModel().getColumn(3).setResizable(false);
        }
        // Set Font the table header
        transitionTable.getTableHeader().setFont(new Font("Cambria", Font.ITALIC+ Font.BOLD, 15));

        // Center content of headers
        ((DefaultTableCellRenderer)transitionTable.getTableHeader().getDefaultRenderer())
        .setHorizontalAlignment(JLabel.CENTER);

        // Center Value in Table
        DefaultTableCellRenderer centerTransition = new DefaultTableCellRenderer();
        centerTransition .setHorizontalAlignment( JLabel.CENTER );
        transitionTable.setDefaultRenderer(String.class, centerTransition );

        TransitionTablePanel.add(transitionTableScrollPane, new java.awt.GridBagConstraints());

        state_transitionPanel.add(TransitionTablePanel);

        sequenceCountingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "Sequence", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Cambria", 1, 22), java.awt.Color.gray)); // NOI18N
        sequenceCountingPanel.setToolTipText("Sequence Number");
        sequenceCountingPanel.setLayout(new java.awt.GridBagLayout());

        sequenceLabel.setFont(new java.awt.Font("Times New Roman", 1, 180)); // NOI18N
        sequenceLabel.setText("1");
        sequenceCountingPanel.add(sequenceLabel, new java.awt.GridBagConstraints());

        state_transitionPanel.add(sequenceCountingPanel);

        stnDiagramPanel.add(state_transitionPanel, java.awt.BorderLayout.NORTH);

        nextStatePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "Next State Table", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Cambria", 1, 22), java.awt.Color.gray)); // NOI18N
        nextStatePanel.setToolTipText("Next State Table");
        nextStatePanel.setLayout(new java.awt.BorderLayout());
        stnDiagramPanel.add(nextStatePanel, java.awt.BorderLayout.CENTER);

        outputTabbedPane.addTab("STN Diagrams", stnDiagramPanel);

        kMapPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "Karnaugh Maps", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Cambria", 1, 24), new java.awt.Color(153, 153, 153))); // NOI18N
        kMapPanel.setToolTipText("Karnaugh Maps");
        kMapPanel.setLayout(new java.awt.GridLayout(1, 0));
        kMapScroll.setViewportView(kMapPanel);

        outputTabbedPane.addTab("Karnaugh Maps", kMapScroll);

        getContentPane().add(outputTabbedPane);

        DSDMenuBar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        fileMenu.setMnemonic('F');
        fileMenu.setText("File");
        fileMenu.setFont(new java.awt.Font("Cambria", 1, 16)); // NOI18N

        newMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        newMenuItem.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        newMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/NewFile.png"))); // NOI18N
        newMenuItem.setMnemonic('N');
        newMenuItem.setText("New Sequence");
        newMenuItem.setToolTipText("Create a new sequence");
        newMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(newMenuItem);

        saveAsMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        saveAsMenuItem.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        saveAsMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/save_icon.png"))); // NOI18N
        saveAsMenuItem.setMnemonic('S');
        saveAsMenuItem.setText("Save As");
        saveAsMenuItem.setToolTipText("Save As (.png, .jpeg, .dsd)");
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveAsMenuItem);
        fileMenu.add(saveAs_showGrouping_Separator);

        showGroupingMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        showGroupingMenuItem.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        showGroupingMenuItem.setSelected(true);
        showGroupingMenuItem.setText("Show Grouping K-Maps");
        showGroupingMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showGroupingMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(showGroupingMenuItem);
        fileMenu.add(showGrouping_connectToHardware_Separator);

        connectTohardwareMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        connectTohardwareMenuItem.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        connectTohardwareMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/Connect arduino.png"))); // NOI18N
        connectTohardwareMenuItem.setMnemonic('C');
        connectTohardwareMenuItem.setText("Connect Hardware");
        connectTohardwareMenuItem.setToolTipText("Connect DSDCircuit");
        connectTohardwareMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectTohardwareMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(connectTohardwareMenuItem);

        DSDMenuBar.add(fileMenu);

        helpMenu.setMnemonic('H');
        helpMenu.setText("Help");
        helpMenu.setFont(new java.awt.Font("Cambria", 1, 16)); // NOI18N

        userManualMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        userManualMenuItem.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        userManualMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/Help Icon.png"))); // NOI18N
        userManualMenuItem.setMnemonic('U');
        userManualMenuItem.setText("User Manual");
        userManualMenuItem.setToolTipText("User Manual of DigitalSD");
        userManualMenuItem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        userManualMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userManualMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(userManualMenuItem);

        aboutMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F2, 0));
        aboutMenuItem.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        aboutMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/About Icon.png"))); // NOI18N
        aboutMenuItem.setMnemonic('A');
        aboutMenuItem.setText("About DSD");
        aboutMenuItem.setToolTipText("About DigitalSD");
        aboutMenuItem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        DSDMenuBar.add(helpMenu);

        setJMenuBar(DSDMenuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsMenuItemActionPerformed
        if(output.isDirectory()){
            
            // instantiate JFileChooser
            JFileChooser saveFile = new JFileChooser(output.dsdDir);
            
            // Filtere of the JFileChooser
            FileNameExtensionFilter dsdFormat = new FileNameExtensionFilter("DSD File", "dsd");
            FileNameExtensionFilter pngFormat = new FileNameExtensionFilter("PNG File", "png");
            FileNameExtensionFilter jpegFormat = new FileNameExtensionFilter("JPEG File", "jpeg");
            
            // set filter of the JFileCHooser
            saveFile.setFileFilter (dsdFormat);
            saveFile.setFileFilter (pngFormat);
            saveFile.setFileFilter (jpegFormat);
            saveFile.setAcceptAllFileFilterUsed (false);
            
            // Set Titlte of the dialog
            saveFile.setDialogTitle("Save As");

            // if clicked on Yes
            if(saveFile.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
            {
                BufferedImage temp1 = createImage(stnDiagramPanel);
                BufferedImage temp2 = createImage(kMapPanel);
                
                File stnFile;
                File kmapFile;
                
                try {

                    if(saveFile.getFileFilter().getDescription().contains("PNG")){

                        stnFile = new File(saveFile.getSelectedFile().getPath()+"_STN.png");
                        ImageIO.write(temp1, "PNG", stnFile);
                        kmapFile = new File(saveFile.getSelectedFile().getPath()+"_Karnaugh Maps.png");
                        ImageIO.write(temp2, "PNG", kmapFile);

                    } else if (saveFile.getFileFilter().getDescription().contains("JPEG")){
                        stnFile = new File(saveFile.getSelectedFile().getPath()+"_STN.jpeg");
                        ImageIO.write(temp1, "JPEG", stnFile);
                        kmapFile = new File(saveFile.getSelectedFile().getPath()+"_Karnaugh Maps.jpeg");
                        ImageIO.write(temp2, "JPEG", kmapFile);

                    } else if (saveFile.getFileFilter().getDescription().contains("DSD")){
                        String filename = saveFile.getSelectedFile().getPath() + ".dsd";
                        String sequenceInfo = "";

                        sequenceInfo = sequenceInfo.concat(String.valueOf(output.numberOfInputs).concat("-"));

                        for (int i = 0; i < output.numberOfInputs; i++) {
                            sequenceInfo = sequenceInfo.concat(output.decimalSequence.get(i)).concat("-");
                        }// end for

                        sequenceInfo = sequenceInfo.concat(output.cycle).concat("-");

                        sequenceInfo = sequenceInfo.concat(String.valueOf(output.optionChoosen).concat("-"));

                        if (output.optionChoosen == 0 ){
                            sequenceInfo = sequenceInfo.concat("0");

                        } else{
                            sequenceInfo = sequenceInfo.concat(output.sequenceOptionField);
                        }// end if

                        // save informations into the file
                        OpenAndSaveFile saveAs = new OpenAndSaveFile();
                        saveAs.write (filename, sequenceInfo);

                        JOptionPane.showMessageDialog(null, "File save successfully in \n"+filename, "SAVED", JOptionPane.INFORMATION_MESSAGE);

                    }// end if

                    output.displayMessage("Files has been saved at this location \n" + saveFile.getSelectedFile().getPath(), "File Saved !!!");


                } // end try 
                catch (IOException | HeadlessException ex) {
                    // feedback message to state that the file is saved
                    JOptionPane.showMessageDialog(null, "Unfortunatey File could not be saved !!!", "NOT SAVED", JOptionPane.INFORMATION_MESSAGE);
                }// end Catch
            }// end if approved
        }// end if directory is existants
    }//GEN-LAST:event_saveAsMenuItemActionPerformed

    private void newMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newMenuItemActionPerformed
        
        // reset inputs values
        output.numberOfInputs = 0;
        output.decimalSequence.clear();
        output.cycle = "";
        output.numberOfBits = 0;
        output.optionChoosen = 0;
        output.sequenceOptionField = "";
        sequenceCountingUp.interrupt();
        
        dispose(); // close cyrrent window
        
        // Reopen input GUI
        final InputForm relaunchApp = new InputForm();
        relaunchApp.setVisible(true);
        relaunchApp.setLocationRelativeTo(null);
        
    }//GEN-LAST:event_newMenuItemActionPerformed

    private void connectTohardwareMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connectTohardwareMenuItemActionPerformed
        
        Thread con = new Thread(){
            @Override public void run(){
                if(output.numberOfBits == 3){
                    if(output.portUsed.isEmpty()){
                        ConnectToArduinoPort connection = new ConnectToArduinoPort();
                        connection.setVisible(true);
                        connection.setLocationRelativeTo(null);
                    } else{
                        ConnectToArduinoPort connection = new ConnectToArduinoPort(output.portUsed);
                        connection.setVisible(true);
                        connection.setLocationRelativeTo(null);
                    }// end if
                    
                }else{
                    output.displayMessage("DigitalSD hardware only supports 3-bits sequence counters so far", "Not Implemented yet !!!");
                }// end if
                
            }// end method run
        };con.start();
    }//GEN-LAST:event_connectTohardwareMenuItemActionPerformed

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed

        // About Menu Item ActionListener
        AboutDSDForm aboutDSD = new AboutDSDForm();
        aboutDSD.setVisible(true);
        aboutDSD.setLocationRelativeTo(null);
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void userManualMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userManualMenuItemActionPerformed

        // User Manual Menu Item ActionListener
        UserManualForm helpScreen = new UserManualForm();
        helpScreen.setLocationRelativeTo( null );
        helpScreen.setVisible( true );
    }//GEN-LAST:event_userManualMenuItemActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
       GetInput.closingWindow(this);
    }//GEN-LAST:event_formWindowClosing

    private void showGroupingMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showGroupingMenuItemActionPerformed
        if(!showGroupingMenuItem.isSelected()){
            karnaughMaps(nextStateTable, null);
        }
        else{
            karnaughMaps(nextStateTable, new Color(204,0,03));
        }
    }//GEN-LAST:event_showGroupingMenuItemActionPerformed

    // Set Icon
    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/digitalsequencedesigner/ImagesDSD/Logo Icon.png")));
    }// end method setIcon
    
    // create image panel a panel
    private BufferedImage createImage(JComponent component) {
        BufferedImage bi = null;
        try {
            int w = component.getWidth();
            int h = component.getHeight();
            bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bi.createGraphics();
            component.paint(g);
        } catch (Exception e) {}// end catch
        
        return bi;
    }// end method createImage

    private class CountingUp extends Thread{
        
        @Override
        public void run(){
            
            try {
                if(output.optionChoosen == 1){
                    
                    // Starter Number of the sequence
                    sequenceLabel.setText(output.sequenceOptionField);
                    Thread.sleep(1000); // delay Counting of one Second

                }// end if
                
                while(true){
                    
                    if (output.cycle.equalsIgnoreCase("Up/Down Counter")){

                            counterSequence = 0;
                            while(counterSequence < output.numberOfInputs){
                                sequenceLabel.setText((String)output.decimalSequence.get(counterSequence));
                                Thread.sleep(1000);// delay Counter of one Second

                                counterSequence++;
                            }
                            counterSequence = output.numberOfInputs - 1;
                            while(counterSequence >= 0){
                                sequenceLabel.setText((String)output.decimalSequence.get(counterSequence));
                                Thread.sleep(1000);
                                
                                counterSequence--;
                            }// end while
                    }
                    else {
                         counterSequence = 0;
                        while(counterSequence < output.numberOfInputs){
                            sequenceLabel.setText((String)output.decimalSequence.get(counterSequence));
                            Thread.sleep(1000);// delay Counter of one Second

                            counterSequence++;
                        }// end while
                    }// end if
                }// end while
            } catch (InterruptedException ex) {}// end catch
        }// end method run
    }//end class CountingUp
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar DSDMenuBar;
    private javax.swing.JPanel TransitionTablePanel;
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JMenuItem connectTohardwareMenuItem;
    private javax.swing.JPanel cycleStatusPanel;
    private javax.swing.JLabel downCounterLabel;
    private javax.swing.JLabel downCounterLogo;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JPanel kMapPanel;
    private javax.swing.JScrollPane kMapScroll;
    private javax.swing.JPanel mainStateDiagramPanel;
    private javax.swing.JMenuItem newMenuItem;
    private javax.swing.JPanel nextStatePanel;
    private javax.swing.JTabbedPane outputTabbedPane;
    private javax.swing.JMenuItem saveAsMenuItem;
    private javax.swing.JPopupMenu.Separator saveAs_showGrouping_Separator;
    private javax.swing.JPanel sequenceCountingPanel;
    private javax.swing.JPanel sequenceInfoPanel;
    private javax.swing.JLabel sequenceLabel;
    private javax.swing.JCheckBoxMenuItem showGroupingMenuItem;
    private javax.swing.JPopupMenu.Separator showGrouping_connectToHardware_Separator;
    private javax.swing.JPanel stateDiagram;
    private javax.swing.JPanel stateDiagramPanel;
    private javax.swing.JScrollPane stateDiagramScroll;
    private javax.swing.JLabel stateMessageLabel;
    private javax.swing.JPanel state_transitionPanel;
    private javax.swing.JPanel stnDiagramPanel;
    private javax.swing.JTable transitionTable;
    private javax.swing.JScrollPane transitionTableScrollPane;
    private javax.swing.JLabel upCounterLabel;
    private javax.swing.JLabel upCounterLogo;
    private javax.swing.JMenuItem userManualMenuItem;
    // End of variables declaration//GEN-END:variables
}
