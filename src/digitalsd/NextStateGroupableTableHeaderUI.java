
package digitalsd;

/**
 * NextStateGroupableTableHeaderUI
 * 
 * From http://www.crionics/com/products/opensource/faq/swing_ex/ swingExamples.html
 * found on www.java2s.com/Code/java/Swing-Component/ GroupHeaderExample.htm
 * 
 * 
 * Modified by: Nobuo Tamemasa
 * @version 1.0 20.10.1998
 * @author Nobuo Tamemasa
 * 
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicTableHeaderUI;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class NextStateGroupableTableHeaderUI extends BasicTableHeaderUI {

    protected NextStateGroupableTableHeader getHeader() {
        return (NextStateGroupableTableHeader) header;
    }// end method getHeader

    @Override
    public void paint(Graphics g, JComponent c) {
        Rectangle clipBounds = g.getClipBounds();
        if (header.getColumnModel().getColumnCount() == 0) {
            return;
        }// end if
        
        int column = 0;
        Dimension size = header.getSize();
        
        Rectangle cellRect = new Rectangle(0, 0, size.width, size.height);
        Map<NextStateColumnGroup, Rectangle> groupSizeMap = new HashMap<>();

        outerLoop: for (Enumeration<TableColumn> enumeration = header.getColumnModel().getColumns(); enumeration.hasMoreElements();) {
            cellRect.height = size.height;
            cellRect.y = 0;
            TableColumn aColumn = enumeration.nextElement();
            List<NextStateColumnGroup> groups = getHeader().getColumnGroups(aColumn);
            int groupHeight = 0;
            
            innerLoop:for (NextStateColumnGroup group : groups) {
                Rectangle groupRect = groupSizeMap.get(group);
                if (groupRect == null) {
                    groupRect = new Rectangle(cellRect);
                    Dimension d = group.getSize(header.getTable());
                    groupRect.width = d.width;
                    groupRect.height = d.height;
                    groupSizeMap.put(group, groupRect);
                }// end if
                
                paintCell(g, groupRect, group);
                groupHeight += groupRect.height;
                cellRect.height = size.height - groupHeight;
                cellRect.y = groupHeight;
            }// end for innerLoop
            
            cellRect.width = aColumn.getWidth();
            if (cellRect.intersects(clipBounds)) {
                paintCell(g, cellRect, column);
            }// end if
            
            cellRect.x += cellRect.width;
            column++;
        }// end for outerLoop
    }// end method paint

    private void paintCell(Graphics g, Rectangle cellRect, int columnIndex) {
        TableColumn aColumn = header.getColumnModel().getColumn(columnIndex);
        TableCellRenderer renderer = aColumn.getHeaderRenderer();
        if (renderer == null) {
            renderer = getHeader().getDefaultRenderer();
        }// end if
        
        Component c = renderer.getTableCellRendererComponent(header.getTable(), aColumn.getHeaderValue(), false, false,
                -1, columnIndex);

        c.setBackground(UIManager.getColor("control"));

        rendererPane.paintComponent(g, c, header, cellRect.x, cellRect.y, cellRect.width, cellRect.height, true);
    }// end paintCell

    private void paintCell(Graphics g, Rectangle cellRect, NextStateColumnGroup cGroup) {
        TableCellRenderer renderer = cGroup.getHeaderRenderer();
        
        if (renderer == null) {
            renderer = getHeader().getDefaultRenderer();
        }// end if

        Component component = renderer.getTableCellRendererComponent(header.getTable(), cGroup.getHeaderValue(), false,
                false, -1, -1);
        rendererPane
                .paintComponent(g, component, header, cellRect.x, cellRect.y, cellRect.width, cellRect.height, true);
    }// end method paintCell

    private int getHeaderHeight() {
        int headerHeight = 0;
        
        TableColumnModel columnModel = header.getColumnModel();
        for (int column = 0; column < columnModel.getColumnCount(); column++) {
            TableColumn aColumn = columnModel.getColumn(column);
            TableCellRenderer renderer = aColumn.getHeaderRenderer();
            if (renderer == null) {
                renderer = getHeader().getDefaultRenderer();
            }// end if

            Component comp = renderer.getTableCellRendererComponent(header.getTable(), aColumn.getHeaderValue(), false,
                    false, -1, column);
            
            int cHeight = comp.getPreferredSize().height;
            
            List<NextStateColumnGroup> groups = getHeader().getColumnGroups(aColumn);
            for (NextStateColumnGroup group : groups) {
                cHeight += group.getSize(header.getTable()).height;
            }// end for
            
            headerHeight = Math.max(headerHeight, cHeight);
        }// end for
        
        return headerHeight;
    }// end method getHeaderHeight

    @Override
    public Dimension getPreferredSize(JComponent c) {
        int width = 0;
        
        for (Enumeration<TableColumn> enumeration = header.getColumnModel().getColumns(); enumeration.hasMoreElements();) {
            TableColumn aColumn = enumeration.nextElement();
            width += aColumn.getPreferredWidth();
        }// end for 
        
        return createHeaderSize(width);
    }// end method getPreferredSize

    private Dimension createHeaderSize(int width) {
        TableColumnModel columnModel = header.getColumnModel();
        width += columnModel.getColumnMargin() * columnModel.getColumnCount();
        
        if (width > Integer.MAX_VALUE) {
            width = Integer.MAX_VALUE;
        }// end if
        
        return new Dimension(width, getHeaderHeight());
    }// end method createHeaderSize

} // end class NextStateGroupableTableHeaderUI