package digitalsd;

/**
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */

import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class OpenAndSaveFile {
    
    // Instances variables
    private String line;
    protected File file;
    private BufferedReader buffered;
    private StringBuilder stringContainer;
    private final GetInput message;

    // Constructor
    public OpenAndSaveFile() {
        this.message = GetInput.getInstance();
    }// end constructor

    /**
     * @param path
     */
    //Read file
    protected void readFile(String path){
        
        try {  				
            buffered = new BufferedReader(new FileReader(path));
            stringContainer  = new StringBuilder();

            // Read each line while the file has a line
            while(true) {
                
                line = buffered.readLine ();
                if (line == null){
                    break;
                }// end if
                stringContainer.append (line).append ("\n");
            }// End While
            
            buffered.close();
        } // try
        catch (FileNotFoundException ex) {
            message.displayMessage("Sorry, Could not found the file", "Not found File");
        }// end catch
        catch (IOException | NullPointerException ex) {}// end catch        
    }// end method readFile
    
    /**
     *
     * @return
     */
    // get the contehnt in the file read
    protected String getFileContent(){
        return stringContainer.toString();
    }// end method getFileContent
    
    /**
     *
     * @param filePath
     * @param fileContent
     */
    // Store data
    protected void write(String filePath, String fileContent){
        try{	
            // Write to File
            file = new File(filePath);
            
            // if file doesnt exists, then create it
            if (!file.exists()) {
                    
                // create field if file do not exist
                file.createNewFile();

                // Saving content to the file created
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                try (BufferedWriter bw = new BufferedWriter(fw)) {
                    bw.write(fileContent);
                }// end try
                
                message.displayMessage("File save successfully in \n" + filePath, "Sequence saved");
            } else{
                
                Toolkit.getDefaultToolkit().beep();
                message.displayMessage("File with this name, exists already,Please give it another naming", "EXISTING FILE ERROR !!!");
            }// end if            
        }// end try
        catch(IOException ioException){}// End catch	
    }// end mwthod write
}// end class OpenAndSaveFile
