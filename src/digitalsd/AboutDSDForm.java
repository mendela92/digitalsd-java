
package digitalsd;

import java.awt.Toolkit;

/**
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */

public class AboutDSDForm extends javax.swing.JFrame {

    public AboutDSDForm() {
        
        // Set design icon
        setIcon();
                
        // Components generated
        initComponents();
        
        // set focus to the button
        closeButton.requestFocus();

    }// end constructor
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        aboutDetailPanel = new javax.swing.JPanel();
        titleLabel = new javax.swing.JLabel();
        authorLabel = new javax.swing.JLabel();
        versionLabel = new javax.swing.JLabel();
        yearLabel = new javax.swing.JLabel();
        descriptionLabel = new javax.swing.JLabel();
        descriptionArea = new javax.swing.JTextArea();
        authorNameLabel = new javax.swing.JLabel();
        studentNoLabel = new javax.swing.JLabel();
        authorStudentNoLabel = new javax.swing.JLabel();
        closeButton = new javax.swing.JButton();
        logoPanel = new javax.swing.JPanel();
        logoLabel = new javax.swing.JLabel();

        setTitle("About DigitalSD");
        setResizable(false);
        setType(java.awt.Window.Type.POPUP);
        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
            }
            public void windowLostFocus(java.awt.event.WindowEvent evt) {
                formWindowLostFocus(evt);
            }
        });

        aboutDetailPanel.setLayout(new java.awt.GridBagLayout());

        titleLabel.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        titleLabel.setForeground(new java.awt.Color(255, 51, 51));
        titleLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/digitalsequencedesigner/ImagesDSD/DigitalSD.png"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 5, 10);
        aboutDetailPanel.add(titleLabel, gridBagConstraints);

        authorLabel.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        authorLabel.setText("Author:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 10, 10);
        aboutDetailPanel.add(authorLabel, gridBagConstraints);

        versionLabel.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        versionLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        versionLabel.setText("Version 1.0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 10, 35);
        aboutDetailPanel.add(versionLabel, gridBagConstraints);

        yearLabel.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        yearLabel.setForeground(new java.awt.Color(255, 51, 51));
        yearLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        yearLabel.setText("© 2015 DigitalSD");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 7);
        aboutDetailPanel.add(yearLabel, gridBagConstraints);

        descriptionLabel.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        descriptionLabel.setText("Description:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 10, 25);
        aboutDetailPanel.add(descriptionLabel, gridBagConstraints);

        descriptionArea.setEditable(false);
        descriptionArea.setColumns(20);
        descriptionArea.setFont(new java.awt.Font("Cambria", 0, 15)); // NOI18N
        descriptionArea.setRows(5);
        descriptionArea.setText("DigitalSD is a BCD Synchronous counter designer implementing State Daigram, \nTransition table, next State table, karnaugh maps as well as logic expressions. \nDigitalSD can also simulate the sequence entered. This Software is a school project \nsupervised by Mr Moyo Vusumuzi at Cape Peninsula University of Technology.");
        descriptionArea.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.ipadx = 20;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 7, 10);
        aboutDetailPanel.add(descriptionArea, gridBagConstraints);

        authorNameLabel.setFont(new java.awt.Font("Cambria", 1, 16)); // NOI18N
        authorNameLabel.setForeground(new java.awt.Color(255, 51, 51));
        authorNameLabel.setText("Nelson Dick Kelechi ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 10);
        aboutDetailPanel.add(authorNameLabel, gridBagConstraints);

        studentNoLabel.setFont(authorLabel.getFont());
        studentNoLabel.setText("Student No:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 10, 10);
        aboutDetailPanel.add(studentNoLabel, gridBagConstraints);

        authorStudentNoLabel.setFont(authorNameLabel.getFont());
        authorStudentNoLabel.setForeground(authorNameLabel.getForeground());
        authorStudentNoLabel.setText("212299859");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 10);
        aboutDetailPanel.add(authorStudentNoLabel, gridBagConstraints);

        closeButton.setFont(new java.awt.Font("Cambria", 0, 12)); // NOI18N
        closeButton.setMnemonic('C');
        closeButton.setText("Close");
        closeButton.setToolTipText("Close");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });
        closeButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                closeButtonKeyPressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        aboutDetailPanel.add(closeButton, gridBagConstraints);

        getContentPane().add(aboutDetailPanel, java.awt.BorderLayout.CENTER);

        logoPanel.setLayout(new java.awt.GridBagLayout());

        logoLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        logoPanel.add(logoLabel, new java.awt.GridBagConstraints());

        getContentPane().add(logoPanel, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        dispose(); // close current Frame
    }//GEN-LAST:event_closeButtonActionPerformed

    private void closeButtonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_closeButtonKeyPressed
        dispose(); // close current Frame
    }//GEN-LAST:event_closeButtonKeyPressed

    private void formWindowLostFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowLostFocus
        dispose(); // close current Frame
    }//GEN-LAST:event_formWindowLostFocus

    // Set Icon
    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/digitalsequencedesigner/ImagesDSD/Logo Icon.png")));
    }// end method setIcon

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel aboutDetailPanel;
    private javax.swing.JLabel authorLabel;
    private javax.swing.JLabel authorNameLabel;
    private javax.swing.JLabel authorStudentNoLabel;
    private javax.swing.JButton closeButton;
    private javax.swing.JTextArea descriptionArea;
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JLabel logoLabel;
    private javax.swing.JPanel logoPanel;
    private javax.swing.JLabel studentNoLabel;
    private javax.swing.JLabel titleLabel;
    private javax.swing.JLabel versionLabel;
    private javax.swing.JLabel yearLabel;
    // End of variables declaration//GEN-END:variables
}// end class AboutDSDForm
