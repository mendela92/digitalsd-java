package digitalsd;

/**
 * NextStateGroupableTableHeaderUI
 * 
 * From http://www.crionics/com/products/opensource/faq/swing_ex/ swingExamples.html
 * found on www.java2s.com/Code/java/Swing-Component/ GroupHeaderExample.htm
 * 
 * 
 * Modified by: Nobuo Tamemasa
 * @version 1.0 20.10.1998
 * @author Nobuo Tamemasa
 * 
 *           DigitalSD
 * @author Nelson Dick KELECHI
 * @author 212299859
 * @Version 1.0
 */

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;


public class NextStateTable extends JPanel{
    
    // Variables
    
    private final GetInput fillNextSateTable;
    
    // ArrayLists
    private ArrayList <String> [] outputs;
    private ArrayList <String> fullSequence; 
    private ArrayList <String> illegalStateNumbers;

    // Strings
    private String tempConversion;
    private String [] decNumber;
    private String [][] qValueNextUpState;
    private String [][] qValuePresentState;
    private String [][] qValueNextDownState;
    
    // Table
    private final JTable nextStateTable;
    
    // Table Model
    private final TableColumnModel cm;
    
    // Column Grouping of Coloumns
    private NextStateColumnGroup stateGroup;
    private NextStateColumnGroup flipFlopUpDown;
    private NextStateColumnGroup flipFlopUpOrDown;
    
    // Group Header of table
    private final NextStateGroupableTableHeader header;
    
    // ScrollPane of table
    private final JScrollPane nextStateTableScroll;
    
    /**
     *  Constructor
     */
    public NextStateTable(){
        
        // Instantiate variables
        this.illegalStateNumbers = new ArrayList <>();
        this.fullSequence = new ArrayList <>();
        this.fillNextSateTable = GetInput.getInstance();

        // Generated Components
        setLayout(new java.awt.BorderLayout());

        // Instantiate table Model
        DefaultTableModel nextStateModel = new DefaultTableModel();
        
        // addColumoutputsnGroup model to the table
        nextStateTable = new JTable( nextStateModel ) {
            @Override
            protected JTableHeader createDefaultTableHeader() {
                return new NextStateGroupableTableHeader(columnModel);
            }// end method createDefaultTableHeader
        };
        
        // Set Font the table header
        nextStateTable.getTableHeader().setFont(new Font("Cambria", Font.ITALIC+ Font.BOLD, 19));

        // Center content of headers
        ((DefaultTableCellRenderer)nextStateTable.getTableHeader().getDefaultRenderer())
            .setHorizontalAlignment(JLabel.CENTER);

        // Setting Table Column headers
        cm = nextStateTable.getColumnModel();
        header = (NextStateGroupableTableHeader)nextStateTable.getTableHeader();
        String [] columnHeaders = {"Present State", "Next State DOWN", "Next State UP", "J-K Flip Flop outputs"};

        nextStateModel.setColumnIdentifiers(createSubHeaders());
        
        if(fillNextSateTable.numberOfBits == 3){
            
            // table model subheader for an up/down counter 
            if(fillNextSateTable.cycle.equalsIgnoreCase("Up/Down Counter")){
                
                columnHeaderGrouping(columnHeaders[0], 1);
                columnHeaderGrouping(columnHeaders[1], 4);
                columnHeaderGrouping(columnHeaders[2], 7);
                OutputHeaderUpAndDown(columnHeaders[3]);
            }// end if

            else{ // table model subheader for an up or down counter 
                
                columnHeaderGrouping(columnHeaders[0], 1);
                
                if (fillNextSateTable.cycle.equalsIgnoreCase("Down Counter")){// Counter Down
                    columnHeaderGrouping(columnHeaders[1], 4);
                    
                } else if (fillNextSateTable.cycle.equalsIgnoreCase("Up Counter")){ // Couonter Up
                    columnHeaderGrouping(columnHeaders[2], 4);
                    
                }// end if
                
                outputHeaderUpOrDown(columnHeaders[3]);
                
                
            }// end if 
            nextStateModel.setNumRows(8);
        }
        else if (fillNextSateTable.numberOfBits == 4){
            if(fillNextSateTable.cycle.equalsIgnoreCase("Up/Down Counter")){
                
                columnHeaderGrouping(columnHeaders[0], 1);
                columnHeaderGrouping(columnHeaders[1], 5);
                columnHeaderGrouping(columnHeaders[2], 9);
                OutputHeaderUpAndDown(columnHeaders[3]);
            }// end if
            else{
                
                columnHeaderGrouping(columnHeaders[0], 1);
                
                if (fillNextSateTable.cycle.equalsIgnoreCase("Down Counter")){// Counter Down
                    columnHeaderGrouping(columnHeaders[1], 5);
                    
                }else if (fillNextSateTable.cycle.equalsIgnoreCase("Up Counter")){ // Couonter Up
                    columnHeaderGrouping(columnHeaders[2], 5);
                    
                }// end if
                
                outputHeaderUpOrDown(columnHeaders[3]);
                
                
            }// end if
            nextStateModel.setNumRows(16);
        }// end if
        
        // Center Value in Table
        DefaultTableCellRenderer centerNextStateTable = new DefaultTableCellRenderer();
        centerNextStateTable.setHorizontalAlignment( JLabel.CENTER );
        nextStateTable.setDefaultRenderer(String.class, centerNextStateTable);
        
        nextStateTable.setRowHeight(30);        // set Table Row Height
        nextStateTable.setEnabled(false);       // Disable table
        nextStateTable.setCursor(new Cursor(Cursor.HAND_CURSOR));
        nextStateTable.setFont(new java.awt.Font("Cambria", 1, 18)); // Set table font
        nextStateTable.setToolTipText("Next State Table");  // Set tool tip of next state table
        
        getIllegalState();  // determine illegal states values
        
        displayNextStateValues();
        
        nextStateTableScroll = new JScrollPane( nextStateTable );
        if(fillNextSateTable.numberOfBits == 3)
            nextStateTableScroll.setPreferredSize(new Dimension( 900,  295));
        
        add( nextStateTableScroll );
    }// end constructors

    // Generating subHeaders
    private Object [] createSubHeaders(){
        Object [] subHeaders;
        int counter = 0;
        
        if(fillNextSateTable.cycle.equalsIgnoreCase("Up/Down Counter")){
            subHeaders = new Object [7*fillNextSateTable.numberOfBits+1];
            subHeaders [0] = "#"; 
            
            
            // creatIing subHeaders
            for (int i = 1; i < subHeaders.length; i++) {
                if(i<= 3*fillNextSateTable.numberOfBits){
                   subHeaders [i] = fillNextSateTable.subHeaderInputsNames()[counter++]; 
                    if(counter == fillNextSateTable.numberOfBits){
                        counter = 0;
                    }// end if
                }// end if
                else{
                    subHeaders [i] = fillNextSateTable.subHeaderOuputNames()[counter++];
                }// end if
                
            }// end for
        }// end if
        else{
            subHeaders = new Object [4*fillNextSateTable.numberOfBits+1];
            
            subHeaders [0] = "#"; 
            
            // creatIing subHeaders
            for (int i = 1; i < subHeaders.length; i++) {
                if(i<= 2*fillNextSateTable.numberOfBits){
                   subHeaders [i] = fillNextSateTable.subHeaderInputsNames()[counter++]; 
                    if(counter == fillNextSateTable.numberOfBits){
                        counter = 0;
                    } // end if
                }// end if
                else{
                    subHeaders [i] = fillNextSateTable.subHeaderOuputNames()[counter++];
                }// end if
            }// end for
        }// end if
        return subHeaders;
        
    }// end method createSubHeaders
  
    
//-------------------------------------Headers and Subheaders of the Next State Table---------------------------------------------//
    
    // Header for up and down
    private void OutputHeaderUpAndDown(String outputHeader){
        
        flipFlopUpDown = new NextStateColumnGroup(outputHeader);
        if(fillNextSateTable.numberOfBits == 3){
           for(int i = 10; i <= createSubHeaders().length-1; i++){

                flipFlopUpDown.addColumn(cm.getColumn(i));
                header.addColumnGroup(flipFlopUpDown);
            } // end for
        }// end if
        else if(fillNextSateTable.numberOfBits == 4){
            for(int i = 13; i <= createSubHeaders().length - 1; i++){

                flipFlopUpDown.addColumn(cm.getColumn(i));
                header.addColumnGroup(flipFlopUpDown);
            }// end for
        }// end if
        
    }// end method OutputHeaderUpAndDown
    
    // Outputs Column Header for up or down Counter
    private void outputHeaderUpOrDown(String state){
        
        
        flipFlopUpOrDown = new NextStateColumnGroup(state);
        if(fillNextSateTable.numberOfBits  == 3){
            for(int i = 7; i < createSubHeaders().length; i++){
            
                flipFlopUpOrDown.addColumn(cm.getColumn(i));
                header.addColumnGroup(flipFlopUpOrDown);
            }// end for
            
        } else if(fillNextSateTable.numberOfBits == 4){
            for(int i = 9; i < createSubHeaders().length; i++){
            
                flipFlopUpOrDown.addColumn(cm.getColumn(i));
                header.addColumnGroup(flipFlopUpOrDown);
            }// end for
        }// end if
    }// end method OutputHeaderUpOrDown
    
    // Method to group subheaders to a header
    private void columnHeaderGrouping(String headerGroup, int group){
        stateGroup = new NextStateColumnGroup(headerGroup);
 
        for (int j = group; j < (group+fillNextSateTable.numberOfBits); j++) {
            stateGroup.addColumn(cm.getColumn(j));
        }// end for
        
        header.addColumnGroup(stateGroup);
    }// end method columnHeaderGrouping
    
//--------------------------------End Headers and Subheaders of the Next State Table--------------------------------------------//
    
    private ArrayList <String> compareSate(String value1, String value2){
        
        ArrayList <String> output = new ArrayList <>();

        try {
            if(value1.equals("0") && value2.equals("0")){
                output.clear();
                output.add("0");
                output.add("X");
                return output;
            } else if(value1.equals("0") && value2.equals("1")){
                output.clear();
                output.add("1");
                output.add("X");
                return output;
            } else if(value1.equals("1") && value2.equals("0")){
                output.clear();
                output.add("X");
                output.add("1");
                return output;
            } else if(value1.equals("1") && value2.equals("1")){
                output.clear();
                output.add("X");
                output.add("0");
                return output;
            } else if(value1.equals("0") && value2.equals("X") || value1.equals("1") && value2.equals("X")){
                output.clear();
                output.add("X");
                output.add("X");
                return output;
            }// end if
        }// end try
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Value error");
        }// end try
        
        
        return null;
    }// end method compareSate
 
    
//-----------------------------------------------------------------------------------------------------------------------------//
    
/*
-------------------------------------------------------Next State Table elements-----------------------------------------------//
*/

    //
    private void getIllegalState(){
       
        
        // Generate a 3 Bits sequence number (0-7)
        generate3BitsLoop:
        if(fillNextSateTable.numberOfBits == 3){
            for(int i = 0; i < 8; i++){
                fullSequence.add(String.valueOf(i));
            }// end for generate3BitsLoop
            
        } else if (fillNextSateTable.numberOfBits == 4){
            for(int i = 0; i < 16; i++){
                fullSequence.add(String.valueOf(i));
            }// end for generate3BitsLoop
        }// end if
        
        
        if(fillNextSateTable.optionChoosen == 2){
            
            // Determine illegal State numbers
            for (String fullSequence1 : fullSequence) {
                if (!fillNextSateTable.decimalSequence.contains(fullSequence1)) {
                    illegalStateNumbers.add(fullSequence1);
                } // end if
            } // end for completeSeqenceLoop
        
        }// end if        
    }//end method completeSequence
    
    // Determine the value in the next state diagram
    private void NextStateValues(){
        
        qValueNextDownState = new String [fullSequence.size()][fillNextSateTable.numberOfBits];
        qValueNextUpState = new String [fullSequence.size()][fillNextSateTable.numberOfBits];
        qValuePresentState = new String [fullSequence.size()][fillNextSateTable.numberOfBits];
        decNumber = new String [fullSequence.size()];
        
        
        stateValueLoop:
        for(int i = 0; i < fullSequence.size(); i++){  
                
                decNumber[i] = fullSequence.get(i); // decimal value of the counter
                
                // set values of present states
                tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt((String) fullSequence.get(i)));
                qValuePresentState [i]  = tempConversion.split("(?!^)");
                
                if(fillNextSateTable.decimalSequence.contains(fullSequence.get(i))){

                    //Set value of next state down
                    switch (fillNextSateTable.cycle) {
                        
                        case "Down Counter": // if cycle is down counter
                            
                            nextStateDown(i);
                            
                            break;
                        case "Up Counter": // if cycle is up counter
                            
                            nextStateUp(i);
                            
                            break;
                        case "Up/Down Counter": // if cycle is up/down counter
                            
                            nextStateUpAndDown(i);
                            
                            break;
                    }// end Switch
                    
                } // end if
                
                // filling the rest of the value according to the option selected
                // - Sequence Only Option
                // - Add starter at 
                // - Attached all illegal State to a specific number
                
                 
                /*  CASE WITH NO STARTER
                *   In this case, the whole sequence will be design according to the sequence number entered
                *   and will automatically assignent don't care value (X) to the state that is number 
                *    in the sequence entered by the user
                */
                
                else if (fillNextSateTable.optionChoosen == 0){
                    
                    dontCareState(i);
                    
                }// end if 
                
                 
                /*  CASE WITH STARTER NUMBER
                *   In this case, a starting number which the sequence enetered should start is included.
                *   This, changes the outputs of the the next State table values because from that value it 
                *   will go into the first number of the sequence entered
                */
                else if (fillNextSateTable.optionChoosen == 1){ // Dealing with numbers out of the sequence
                    
                    // checking if a starter number is equal to the current number
                    if (fullSequence.get(i).equals(fillNextSateTable.sequenceOptionField)){

                        starterNumberOption(i);

                    }// end if 
                    else {// if number is not athe starter then it is a do't Care 
                        
                        dontCareState(i);
                        
                    }// end if
                    
                }// end if  
                
                /*  CASE WITH ILLEGAL STATES ATTACHED TO A NUMBER
                *   In this case, Numbers that are not part of the sequence will be always refered this 
                *   specific number when countingthe sequence
                *
                */
                
                else if (fillNextSateTable.optionChoosen == 2){ // Dealing with illegal States case
                    if(illegalStateNumbers.contains(fullSequence.get(i))){

                        // illegal State up and down are same values
                        tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt(fillNextSateTable.sequenceOptionField));
                        qValueNextDownState[i] = tempConversion.split("(?!^)");
                        qValueNextUpState[i] = tempConversion.split("(?!^)");

                }//end if 
                    
            }// end if                
        }// end for stateValueLoop
    }// end method
    
    
    private void nextStateUp(int i){
        
        // check index of an element in arrayList of sequence if it is the index og the last number of the sequence
        if (fillNextSateTable.decimalSequence.indexOf(fullSequence.get(i)) == fillNextSateTable.decimalSequence.size()-1){

            // if true then, the next state up is the first number of the input sequence
            tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt((String)fillNextSateTable.decimalSequence.get(0)));
            qValueNextUpState[i] = tempConversion.split("(?!^)");

        }
        else {
            // else get index of the current number in the input arrayList and addColumnGroup 1 to the input arrayList to get the value of the next state up 
            tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt((String)fillNextSateTable.decimalSequence.get(fillNextSateTable.decimalSequence.indexOf(fullSequence.get(i)) + 1)));
            qValueNextUpState[i] = tempConversion.split("(?!^)");

        }// end if  
        
    }// end method nextStateUp
    
    
    private void nextStateDown(int i){
        
        // checking if the index positon is the first of the arrayList
        if (fillNextSateTable.decimalSequence.indexOf(fullSequence.get(i)) == fillNextSateTable.decimalSequence.size()-1){

            // if true then next state down is the last value of the input sequence number
            tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt((String) fillNextSateTable.decimalSequence.get(0)));
            qValueNextDownState[i] = tempConversion.split("(?!^)");

        } else{ // Set other next State down of the sequence

            // else get current position of the value in arrayList input and subsract 1 to get the vqlue of the next state down
            tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt((String) fillNextSateTable.decimalSequence.get(fillNextSateTable.decimalSequence.indexOf(fullSequence.get(i)) + 1)));
            qValueNextDownState[i] = tempConversion.split("(?!^)");

        }// end if
    }  // end method nextStateDown
    
    
    private void nextStateUpAndDown(int i){
        
        // check index of an element in arrayList of sequence if it is the index og the last number of the sequence
        if (fillNextSateTable.decimalSequence.indexOf(fullSequence.get(i)) == fillNextSateTable.decimalSequence.size()-1){

            // if true then, the next state up is the first number of the input sequence
            tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt((String)fillNextSateTable.decimalSequence.get(0)));
            qValueNextUpState[i] = tempConversion.split("(?!^)");

        } else {
            // else get index of the current number in the input arrayList and addColumnGroup 1 to the input arrayList to get the value of the next state up 
            tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt((String)fillNextSateTable.decimalSequence.get(fillNextSateTable.decimalSequence.indexOf(fullSequence.get(i)) + 1)));
            qValueNextUpState[i] = tempConversion.split("(?!^)");

        }// end if  
        
        // checking if the index positon is the first of the arrayList
        if (fillNextSateTable.decimalSequence.indexOf(fullSequence.get(i)) == 0){

            // if true then next state down is the last value of the input sequence number
            tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt((String) fillNextSateTable.decimalSequence.get(fillNextSateTable.decimalSequence.size()-1)));
            qValueNextDownState[i] = tempConversion.split("(?!^)");

        } else{ // Set other next State down of the sequence

            // else get current position of the value in arrayList input and subsract 1 to get the vqlue of the next state down
            tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt((String) fillNextSateTable.decimalSequence.get(fillNextSateTable.decimalSequence.indexOf(fullSequence.get(i)) - 1)));
            qValueNextDownState[i] = tempConversion.split("(?!^)");  
        }// end if
    }// end nextStateUpAndDown
    
    
    private void dontCareState(int i){
        
        switch (fillNextSateTable.cycle) {
            case "Up/Down Counter":  // next state up/down

                for(int j = 0; j < fillNextSateTable.numberOfBits; j++){
                    qValueNextUpState[i][j] = "X";
                    qValueNextDownState[i][j] = "X";
                }// end for
                
                break;
        
            case "Up Counter": // Next state up

                for(int j = 0; j < fillNextSateTable.numberOfBits; j++){
                    qValueNextUpState[i][j] = "X";
                }// end for
                
                break;

            case "Down Counter":  // Next state down

                for(int j = 0; j < 3; j++){
                    qValueNextDownState[i][j] = "X";
                }// end for
                
                break;
        }// end switch
    }// end method dontCareState
    

    private void starterNumberOption(int i){
        
        switch (fillNextSateTable.cycle) {
            
            case "Down Counter":
                
                tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt((String)fillNextSateTable.decimalSequence.get(fillNextSateTable.decimalSequence.size()-1)));
                qValueNextDownState[i] = tempConversion.split("(?!^)");
                
                break;
            case "Up Counter":
                
                tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt((String)fillNextSateTable.decimalSequence.get(fillNextSateTable.decimalSequence.indexOf(fullSequence.get(i)) + 1)));
                qValueNextUpState[i] = tempConversion.split("(?!^)");
                
                break;
            case "Up/Down Counter":
                
                tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt((String)fillNextSateTable.decimalSequence.get(fillNextSateTable.decimalSequence.indexOf(fullSequence.get(i)) + 1)));
                qValueNextUpState[i] = tempConversion.split("(?!^)");
                
                tempConversion = fillNextSateTable.convertDecToBin(Integer.parseInt((String)fillNextSateTable.decimalSequence.get(fillNextSateTable.decimalSequence.size()-1)));
                qValueNextDownState[i] = tempConversion.split("(?!^)");
                
                break;
        }// end switch
    } // end method starterNumberOption
    
    // Set values of Qn and Qn+1 and Outputs of the 
    private void displayNextStateValues(){
        
        NextStateValues();  // determine next state table values
        
        // setValues to Table
        for(int rowCount = 0; rowCount < fullSequence.size(); rowCount++){ // Rows of the next state table

            // Set value of equivalent decimal number
            nextStateTable.setValueAt(decNumber[rowCount], rowCount, 0);
                    
            for(int columnCount = 0; columnCount < fillNextSateTable.numberOfBits; columnCount++){
                for(int presentStateCount = 1; presentStateCount <= fillNextSateTable.numberOfBits; columnCount++){
                    nextStateTable.setValueAt(qValuePresentState[rowCount][columnCount], rowCount, presentStateCount);
                    presentStateCount++;      
                }// end for
                
                if(fillNextSateTable.cycle.equalsIgnoreCase("Up/Down Counter")){
                    columnCount = 0;
                    for(int downStateCount = fillNextSateTable.numberOfBits+1; downStateCount <= (2*fillNextSateTable.numberOfBits); columnCount++){
                        nextStateTable.setValueAt(qValueNextDownState[rowCount][columnCount], rowCount, downStateCount);
                        downStateCount++;
                    }// end for

                    columnCount = 0;
                    for(int upStateCount = (2*fillNextSateTable.numberOfBits)+1; upStateCount <= (3*fillNextSateTable.numberOfBits); columnCount++){
                        nextStateTable.setValueAt(qValueNextUpState[rowCount][columnCount], rowCount, upStateCount);
                        upStateCount++;
                    }// end for
                    
                    // Outputs of Js and Ks
                    columnCount = 0;
                    for(int JKColumnCount = (3*fillNextSateTable.numberOfBits)+1; JKColumnCount <= (7*fillNextSateTable.numberOfBits); JKColumnCount+=4){
                        nextStateTable.setValueAt(compareSate(qValuePresentState[rowCount][columnCount], qValueNextDownState[rowCount][columnCount]).get(0), rowCount, JKColumnCount );
                        nextStateTable.setValueAt(compareSate(qValuePresentState[rowCount][columnCount], qValueNextUpState[rowCount][columnCount]).get(0), rowCount, JKColumnCount+1 );
                        nextStateTable.setValueAt(compareSate(qValuePresentState[rowCount][columnCount], qValueNextDownState[rowCount][columnCount]).get(1), rowCount, JKColumnCount+2 );
                        nextStateTable.setValueAt(compareSate(qValuePresentState[rowCount][columnCount], qValueNextUpState[rowCount][columnCount]).get(1), rowCount, JKColumnCount+3 );   
                        columnCount++; 
                    }// end for
                    
                }// end if up and down counter
                
                
                else {
                    columnCount = 0;
                    for(int upStateCount = fillNextSateTable.numberOfBits+1; upStateCount <= (2*fillNextSateTable.numberOfBits); columnCount++){
                        if(fillNextSateTable.cycle.equalsIgnoreCase("Up Counter")){
                            nextStateTable.setValueAt(qValueNextUpState[rowCount][columnCount], rowCount, upStateCount);
                        }
                        else if(fillNextSateTable.cycle.equalsIgnoreCase("Down Counter")){
                            nextStateTable.setValueAt(qValueNextDownState[rowCount][columnCount], rowCount, upStateCount);
                        }
                        upStateCount++;
                    }// end for
                    
                    columnCount = 0;
                    for(int JKColumnCount = (2*fillNextSateTable.numberOfBits)+1; JKColumnCount <= (4*fillNextSateTable.numberOfBits); JKColumnCount+=2){
                        if(fillNextSateTable.cycle.equalsIgnoreCase("Up Counter")){
                            nextStateTable.setValueAt(compareSate(qValuePresentState[rowCount][columnCount], qValueNextUpState[rowCount][columnCount]).get(0), rowCount, JKColumnCount );
                            nextStateTable.setValueAt(compareSate(qValuePresentState[rowCount][columnCount], qValueNextUpState[rowCount][columnCount]).get(1), rowCount, JKColumnCount+1 );
                        }
                        else{
                            nextStateTable.setValueAt(compareSate(qValuePresentState[rowCount][columnCount], qValueNextDownState[rowCount][columnCount]).get(0), rowCount, JKColumnCount );
                            nextStateTable.setValueAt(compareSate(qValuePresentState[rowCount][columnCount], qValueNextDownState[rowCount][columnCount]).get(1), rowCount, JKColumnCount+1 );
                        }// end for
                           
                        columnCount++; 
                    }// end for
                }// end if 
            }// end for
        }// end for
    }// end method DisplayData
    
    // Store J and Ks in arraylsit to populate Karnaugh Maps
    protected ArrayList<String> [] getValueInTable(){
        
        int outputPosition;
        
        if(fillNextSateTable.cycle.equals("Up/Down Counter")){
            
            outputPosition = 3*fillNextSateTable.numberOfBits+1;
            if(fillNextSateTable.numberOfBits == 3){
               outputs = new ArrayList[2*fillNextSateTable.numberOfBits];
               
            } else if(fillNextSateTable.numberOfBits == 4){
                outputs = new ArrayList[4*fillNextSateTable.numberOfBits];
            }// end if
            
        } else{
            outputPosition = 2*fillNextSateTable.numberOfBits+1;
            outputs = new ArrayList[2*fillNextSateTable.numberOfBits];
        }// end if
        
        for (int outputCount = 0; outputCount < outputs.length; outputCount++) {
            outputs [outputCount] = new ArrayList<>();
            for (int rowCount = 0; rowCount < fullSequence.size(); rowCount++) {
                 if(fillNextSateTable.cycle.equalsIgnoreCase("Up/Down Counter") ){
                    if(fillNextSateTable.numberOfBits == 3){
                        outputs [outputCount].add( nextStateTable.getValueAt(rowCount, outputPosition).toString() );
                        outputs [outputCount].add( nextStateTable.getValueAt(rowCount, outputPosition+1).toString() );

                    } else if(fillNextSateTable.numberOfBits == 4){
                        outputs [outputCount].add( nextStateTable.getValueAt(rowCount, outputPosition).toString() );
                    }// end if

                 }else{
                    if(fillNextSateTable.numberOfBits == 3){
                        outputs [outputCount].add( nextStateTable.getValueAt(rowCount, outputPosition).toString() );

                    } else if(fillNextSateTable.numberOfBits == 4){
                        outputs [outputCount].add( nextStateTable.getValueAt(rowCount, outputPosition).toString() );
                    }// end if

                 }// end for
            }// end if

            if(fillNextSateTable.cycle.equalsIgnoreCase("Up/Down Counter")){
                if(fillNextSateTable.numberOfBits == 3 && outputPosition < (7*fillNextSateTable.numberOfBits)-1){
                    outputPosition=outputPosition+2;
                }
                else if(fillNextSateTable.numberOfBits == 4){
                    outputPosition++;
                }// end if

            }else{
                outputPosition++;
            }// end if 
        }// end for    
 
        return outputs;
    }// end method getValueInTable
   }// end class NextStateTable